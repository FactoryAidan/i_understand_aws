const Pkijs = require('pkijs')
const Asn1js = require('asn1js')

module.exports	=	class certificateHelper {

	static example_pem	=	`
	-----BEGIN CERTIFICATE-----
	MIIDwTCCAqkCCQCkyYMlP5Yv4zANBgkqhkiG9w0BAQsFADCBsjELMAkGA1UEBhMC
	VVMxEzARBgNVBAgMCkNhbGlmb3JuaWExDzANBgNVBAcMBklydmluZTELMAkGA1UE
	CgwCRkExSTBHBgNVBAMMQDhkMjdmNDZlY2U2YWI0ZjQ0MDVlMzczZWQ5MzRkNGMy
	NTQ5NmNlOTc5YWNmNzU4OWVlMTE4NmY1NGRjZjdlYjMxJTAjBgkqhkiG9w0BCQEW
	FkFpZGFuQF9fX19fX19fX19fXy5jb20wHhcNMjAwODI3MDY1MzExWhcNMzAwODI1
	MDY1MzExWjCBkTELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExDzAN
	BgNVBAcMBklydmluZTELMAkGA1UECgwCRkExKDAmBgNVBAMMH3RoaXNfaXNfbXlf
	dGhpbmdzX3NlcmlhbF9udW1iZXIxJTAjBgkqhkiG9w0BCQEWFkFpZGFuQF9fX19f
	X19fX19fXy5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDGK0kC
	TB1GJ5F8Lf1wVKEIdc/2ukXGKveLDvBAYp/gixP8A85XNsJf45A8coQmljNTehx2
	yb09umW+L7eueTxzpeWfCRCv/pHJnDRD2RUfh+8udSwSOD9bJC+BUiEMt7MQ7dmL
	6EqYDXn26DYKnxwldRy3XJzCg5XfahgfgJjEN6WTN/hwJwVPk1n/86F8EgxZ+n1N
	qdW70elvhXT4b4bB3hddEQM6gzmprYU1HHbRn1BeKHhngc60cEvD7RgO2LsHcehk
	RObvXiCZJeTtsLI+bXEQU5vUuLhwVb879PExiDLr4T35qYQmIsfxbqLvy0/yaD+r
	iZU/NV1nux90/lrZAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAC2PsFZXtU2Hskd0
	omPrCIVYfKrqaI9Y/R+a8bofhYjJpF/SZHMUq7tCdVV1WJtKri1fF5m1bVZ5ik9t
	5J+VEFvf6gkx65DCP6VaMl/YWV6bVxTEcT0K8HsY3w1u4PqCuShqaSUVM8KvxsqO
	sVUCiq2rpIL1wWO9PxWXYNx/9Yg1w71pmpsfz7BcRZx614FlCMBDGtxn0tslmRah
	3YRhaI33y+7llh6dhJ4qlsGZsjuY53XvqRTxSDUFGno/FCfr8y8RwEXtnwMIpAev
	40DXaQ2xFvFiWm27ElbOaPIy+4QqbtXVLNLkMpJyjMq8Ybp5dcqhQ6HSDty26SHM
	RO0Jrrs=
	-----END CERTIFICATE-----`.trim();

	// IOD Repository - Object Identifiers
	map_typecode_to_description	=	{
		'2.5.4.6'				:	'country_code',
		'2.5.4.8'				:	'state',
		'2.5.4.7'				:	'city',
		'2.5.4.10'				:	'organization',
		'2.5.4.3'				:	'common_name',
		'1.2.840.113549.1.9.1'	:	'email',
	};

	constructor(given_certificate_as_pem_string){
		if( typeof given_certificate_as_pem_string !== 'string' ){
			throw 'Certificate must be a PEM String';
		}
	
		this.as_pki_object	=	this.decodePemToPki(given_certificate_as_pem_string);
	}

	decodePemToPki(certificate_as_pem_string) {

		//	A PEM is Base64 translation of the x509 ASN.1 keys.
		//	https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail
		//	The PEM format is 'DER' format (which is a binary format).
		//	However, the 'DER' format then also Base64 encoded to become PEM.
		//	The decoding effort is as follows:

		//	Clean off the header & footer text - Giving us just Base64 encoded string
		const format_base64	=	certificate_as_pem_string.replace(/(-----(BEGIN|END) CERTIFICATE-----|[\n\r])/g, '');

		//	Decode from Base64 - Giving us just Distinguished Encoding Rules(DER) format being held in a binary buffer.
		const format_der	=	Buffer.from(format_base64,'base64');

		//	Convert Distinguished Encoding Rules(DER) format to Basic Encoding Rules(BER), also being held in a binary buffer.
		const format_ber	=	new Uint8Array(format_der).buffer;

		//	Convert Basic Encoding Rules(BER) format into Abstract Syntax Notation One(ASN1)
		const format_asn1	=	Asn1js.fromBER(format_ber);

		//	Convert Abstract Syntax Notation One(ASN1) to Public Key Infrastructure(PKI)
		const format_pki	=	new Pkijs.Certificate({ schema: format_asn1.result });

		//	Jesus, we can read it.
		return format_pki;

	}//func

	getProperties(pki_object=this.as_pki_object){

		const output_properties	=	{};

		pki_object.subject.typesAndValues.forEach((typeAndValue,_i,_a)=>{
			const type		=	typeAndValue.type;
			const type_name	=	this.map_typecode_to_description[type];
			const value		=	typeAndValue.value.valueBlock.value;

		//	console.log( type,type_name, value );	//	Debugging Only

			output_properties[type_name]	=	value;
	
		});

		return output_properties;

	}//func

}//class

//	Usage Example:
//	console.log( new certificateHelper(certificateHelper.example_pem).getProperties() );