const crypto	=	require('crypto');
const FS		=	require('fs');
class AwsCognitoHelper {

	/*
	* https://docs.aws.amazon.com/cognito/latest/developerguide/signing-up-users-in-your-app.html#cognito-user-pools-computing-secret-hash
	* @param	params.user_pool_app_client_id		{string}	'165pbal6micdi3de34nn6bbbiem',
	* @param	params.user_pool_app_client_secret	{string}	'1cknq6icq1h87fuvgt1uh1o3irchsa6lc1olbkl050qc2j8dlmia'
	* @param	params.user_pool_user_name			{string}	'SomeUser@NameEmail.com',
	* @returns										{string}	The hashed secret
	*/
	static calculateSecretHash(params){
	//	DO NOT, UNDER ANY CIRCUMSTANCES, CHANGE THIS FUNCTION's CODE !!!
	//	It works as intended. Stay out. Period.
		const data_to_hash		=	[params.user_pool_user_name,params.user_pool_app_client_id].join('');
		const hmac				=	crypto.createHmac('sha256', params.user_pool_app_client_secret );
		const hash				=	hmac.update(data_to_hash);
		const aws_secret_hash	=	hash.digest('base64');

		return aws_secret_hash;
	}

	static saveJsonWebTokens(given_token_object,given_path='./scripts/Cognito/user.authenticated/.users_current_token'){
		return FS.writeFileSync(given_path,JSON.stringify(given_token_object));
	}

	static getJsonWebTokens(given_path=`${process.cwd()}/scripts/Cognito/user.authenticated/.users_current_token`){
		const	UsersAuthenticationResult	=	JSON.parse( FS.readFileSync(given_path,'utf8') );
		return UsersAuthenticationResult;
	}

}//class

module.exports	=	{
	AwsCognitoHelper,
};