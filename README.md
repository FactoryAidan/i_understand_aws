# The purpose of this repository

...is to show that with documentation, after a little sit-down chat, Cognito is not so bad.

We can all agree that AWS' Cognito documentation is 💩 . Some would say the entire system is terrible. With experience, AWS is quite an amazing tool.

# It's because you didn't FUCKING READ.

Please read this readme.

This is all WAY easier than you think.

# Assumptions

#### AWS Console (web-based)
We're assuming you have already

- Setup a *User Pool* inside of Cognito.
- Added an *App Client* to that User Pool and know the *App Client*'s:
	- **App client id**
	- **App client secret**
- Checked the box in that *App Client* to allow `USER_SRP_AUTH`.

For right now, you may be wondering. No, you do not need a *Identity Pool* (aka. Federated Identities) created for these scripts to work with your *User Pool*.
All that can happen on a different day.

## Basic Setup of this

```
git clone git clone https://FactoryAidan@bitbucket.org/FactoryAidan/i_understand_aws.git
cd i_understand_aws
npm install
cp .env.sample .env
```
👉 Open `/.env` and put the values from AWS Cognito's Web-Based Console into any line that doesn't start with a `#` *(meaning it's a commented line)*.

### PLEASE (know about `.env`)
...be aware that when you see a line in this codebase like `process.env.A_TEST_EMAIL_ADDRESS`, do not change the value of that line.
Save yourself the headache and instead, open your `.env` file and change the `A_TEST_EMAIL_ADDRESS` value there. It will propagate throughout this codebase that way.

# 🟢 Running these scripts

### 📑 Be in the project root `/` or the paths won't work right.

## 👉 You can use `npm run`
```
npm run signUp_signInWithSRP
```

## 👉 You can use `terminal`'s Node.js

#### ℹ️ User signup flow (so you can login)
```
node ./scripts/Congnito/user/signUp/signUp.js							# You get a confirmation code via email
node ./scripts/Congnito/user/signUp/signUp.confirm.js					# You use that confirmation code here
node ./scripts/Congnito/user/signUp/signUp.resendConfirmationCode.js	# You understand
node ./scripts/Congnito/user/signIn/username_password/forgotPassword.js	# You get a reset code via email
```
That creates a user inside of Cognito. At this point, the user will exist but is not signed in.
You must now login 👇.

#### ℹ️ User login flow (so you can do logged-in things)
```
node ./scripts/Congnito/user/signIn/username_password/initiateAuth.js
node ./scripts/Congnito/user/signIn/secure_remote_password/initiateAuth-user_srp_auth.using-library.js
```
You'll get back a JSON object as a response. Get the `AuthenticationResult.AccessKey` property from that JSON response and that'll be your *AccessKey* on all further calls from here-on-out.

For the record, this *AccessKey* is a JSON Web Token (JWT) in case you'd like to know; you can decode it and it tells you things. See the [Token Decoder](https://jwt.io/#debugger-io) if you're curious.

#### ℹ️ Logged-in calls (a.k.a. calls with your `AccessKey` from 👆)
```
node ./scripts/Congnito/user.with_access_token/getUser.js
```

#### 🟡 FYI

Each script declares a `params` object at the top of the script.
While we intend to have app parameters read from `.env` definitions, be aware of this. You may need to change a value in `params` inside of the specific script you're executing.

#### `params` example:
`signUp.confirm.js` calls need to know the `Confirmation Code` sent to the email address if the user *(usually the user email you put in your `.env` file)*.
The confirmationCode changes after every use, so you'll have to update the script.
Put the new `Confirmation Code` from the email inside `params` in `signUp.confirm.js` like this:

```
const params	=	{
	ConfirmationCode:	'623119',
	Username		:	process.env.A_TEST_EMAIL_ADDRESS,
};
```

Don't expect to just blindly run scripts. Put the necessary parameters in each script when they are needed.