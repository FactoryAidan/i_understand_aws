const CH	=	require('../CertificateHelper.js');

const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

(async ()=>{

	const function_params = {
		certificateId	:	'b33b8eef4a645d41e2fea92a6d803d293ee3b3f86df0b73fb1802188d47f9161',
	};

	const response	=	await IOT.describeCertificate(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	const certificate_properties	=	new CH(response.certificateDescription.certificatePem).getProperties();

	console.log('CERTIFICATE:',certificate_properties);

})();
