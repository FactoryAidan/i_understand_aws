const {AwsCognitoHelper}=	require('../helpers/AWS');
const fetch				=	require('node-fetch');

//	Create Request URL + Parameters
var Url	=	new URL('https://history.api.violux.com/week');
Url.searchParams.append('serialNumber','luma_499');
Url.searchParams.append('dateUTC','2021-03-01T00:00:00.000Z');
//	Url.searchParams.append('dateUTC','2021-03-14T00:00:00.000Z');
//	Url.searchParams.append('dateUTC','2021-03-14T17:57:48.893Z');

//	Create Request Headers
const headers	=	{
	'CognitoAuth'	:	AwsCognitoHelper.getJsonWebTokens().IdToken,
}

//	Execute Request
fetch(Url.toString(),{
    headers,
})
	.then(response=>response.json())
	.then(function(response){
		console.log(response);
	})
;
