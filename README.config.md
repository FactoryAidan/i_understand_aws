## Multiple ways to get configuration values into the `aws-sdk`:

#### Create a `.env` file

`aws-sdk` will load your credentials from the `process.env` global object.

```
require('dotenv').config();
console.log( process.env );	//	👈	Your config will be available here.
//	And now this will work
const CISP		=	new AWS.CognitoIdentityServiceProvider();
```

#### Create a `.json` file:

```
const AWS		=	require('aws-sdk');
AWS.config.loadFromPath('./.config_aws_auth.json');
//	And now this will work
const CISP		=	new AWS.CognitoIdentityServiceProvider();
```

#### Assign them inline

```
const AWS		=	require('aws-sdk');
AWS.config.Cognito	=	{
	accessKeyId		:	'YOUR_AWS_ACCESS_KEY_ID',		//	Put your string here
	secretAccessKey	:	'YOUR_AWS_SECRET_ACCESS_KEY',	//	Put your string here
	region			:	'YOUR_AWS_REGION',				//	Put your string here
};
//	And now this will work
const CISP		=	new AWS.CognitoIdentityServiceProvider();
```