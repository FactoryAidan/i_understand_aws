const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.cloudformation	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const CloudFormation	=	new AWS.CloudFormation();

const template_json		=	require(`${__dirname}/test_template.json`);

(async ()=>{

	const function_params = {
		TemplateBody	:	JSON.stringify(template_json),	//	String
	//	TemplateURL: 'STRING_VALUE'		//	String
	};
//	console.log(function_params);	return;	//	Debugging Only
	const response	=	await CloudFormation.validateTemplate(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('Response:',response);

})();
