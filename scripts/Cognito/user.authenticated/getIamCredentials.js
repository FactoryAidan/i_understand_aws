const Dotenv	=	require('dotenv');	Dotenv.config();
const fetch		=	require('node-fetch');
const JWT		=	require('jsonwebtoken');
const FS		=	require('fs');
const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

//	---------------------------------------------------------------------------------
//	Get our saved & current JSON Web Token from normally signIn() to Cognito UserPool
//	---------------------------------------------------------------------------------
const	UsersAuthenticationResult	=	JSON.parse( FS.readFileSync(`${__dirname}/.users_current_token`,'utf8') );

//	-----------------------------------------------------------------
//	Set User's UserPool JSON Web Token as our IdentityPool credential
//	-----------------------------------------------------------------

//	*	Construct a String representation the Identity Provider
//	These two yield identical results:
//	First is faster but second validates the JSON Web Token expiry.
//	1.)
//	const aws_cognito_identity_providerName	=	`cognito-idp.${process.env.AWS_REGION}.amazonaws.com/${process.env.AWS_COGNITO_USER_POOL_ID}`;
//	2.)
	const Jwt						=	JWT.decode(UsersAuthenticationResult.IdToken);
	const identity_provider_name	=	Jwt.iss.replace(/^https:\/\//,'');	//	aka. Issuer Name



//	Create Logins Mapping of Identity Providers to their access Tokens for this User.
//	Give that Map to the Identity Pool.
const Logins	=	{};
Logins[identity_provider_name]	=	UsersAuthenticationResult.IdToken;


AWS.config.region		=	process.env.AWS_REGION;
AWS.config.credentials	=	new AWS.CognitoIdentityCredentials({
	IdentityPoolId	:	process.env.AWS_COGNITO_IDENTITY_POOL_ID,
	Logins			:	Logins,
});

//	----------------------------------------------------------------------
//	Exchange UserPool IdToken for IdentityPool's temporary IAM credentials
//	----------------------------------------------------------------------
// AWS.config.credentials.get(function(error,data){
// 
// 	if( error )	console.log(error);
// 
// 	console.log({
// 		identityId		:	AWS.config.credentials.identityId,
// 		accessKeyId		:	AWS.config.credentials.accessKeyId,
// 		secretAccessKey	:	AWS.config.credentials.secretAccessKey,
// 		sessionToken	:	AWS.config.credentials.sessionToken,
// 	});
// 
// });

(async ()=>{

//	console.log( AWS.config.credentials.get() );
	const iam_credentials	=	await AWS.config.credentials.getPromise().then(function(){
		//	Credentials are refreshed on existing Object.
		const le_creds	=	{
			identityId		:	AWS.config.credentials.identityId,
			accessKeyId		:	AWS.config.credentials.accessKeyId,
			secretAccessKey	:	AWS.config.credentials.secretAccessKey,
			sessionToken	:	AWS.config.credentials.sessionToken,
			expired			:	AWS.config.credentials.expired,
			expireTime		:	AWS.config.credentials.expireTime,
		};
	//	console.log(le_creds);
		return le_creds;
	}).catch(error=>console.log(error));

	console.log(iam_credentials);

})();
