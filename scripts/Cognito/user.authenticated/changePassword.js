const Dotenv	=	require('dotenv');	Dotenv.config();
const FS		=	require('fs');
const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

const	UsersAuthenticationResult	=	JSON.parse( FS.readFileSync(`${__dirname}/.users_current_token`,'utf8') );

const params = {
	AccessToken	:	UsersAuthenticationResult.AccessToken,
	RefreshToken:	UsersAuthenticationResult.RefreshToken,
	IdToken		:	UsersAuthenticationResult.IdToken,
};

(async ()=>{

	const tmp_params = {
		AccessToken		:	params.AccessToken,
		PreviousPassword:	process.env.A_TEST_PASSWORD,
		ProposedPassword:	'YourNewPassword_1'
	};

	const response	=	await CISP.changePassword(tmp_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
