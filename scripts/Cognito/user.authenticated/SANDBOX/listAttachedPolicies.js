const Dotenv	=	require('dotenv');	Dotenv.config();
const FS		=	require('fs');
const fetch		=	require('node-fetch');
const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

const	UsersAuthenticationResult	=	JSON.parse( FS.readFileSync(`${__dirname}/../.users_current_token`,'utf8') );

	const api_base_url	=	'https://0q2t0jnx64.execute-api.us-west-2.amazonaws.com/production';
//	const api_base_url		=	'https://iot.api.violux.com';
const params	=	{
	cognitoIdentityId	:	'us-west-2:36f11e36-f2bb-45de-9ded-ece69551a623',	//	Matti
//	cognitoIdentityId	:	'us-west-2:e12b2f4f-aa98-4705-aeed-527b934e7862',	//	Aidan
};

(async ()=>{

	const url	=	`${api_base_url}/cognito_identities/${params.cognitoIdentityId}/list_attached_policies`;

	console.log('Requesting URL:',url);

	fetch(url,{
		headers	:	{
			'X-Cognito-IdToken'	:	UsersAuthenticationResult.IdToken,
		},
		method	:	'GET',
	})
	.then(function(response){
		console.log('statusCode:',response.status);
		return response.text();
	})
	.then(function(response){
		console.log('Response Text:',response );
	})
	.catch(function(error){console.log(error)});

})();
