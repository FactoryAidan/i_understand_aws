require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);
const CognitoAuthTokens		=	AwsCognitoHelper.getJsonWebTokens();

//	Allows you to pass in arguments in the command line.
const yargs					=	require('yargs/yargs');
const {hideBin}				=	require('yargs/helpers');
const argv					=	yargs( hideBin(process.argv) ).argv;

(async ()=>{

	const params = {
		AccessToken		:	CognitoAuthTokens.AccessToken,
		AttributeName	:	argv.attribute || 'email',
	};

	const response	=	await CISP.getUserAttributeVerificationCode(params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();

//	CLI Example
//	node ./scripts/Cognito/user.authenticated/updateEmail/resendConfirmationCode.js
