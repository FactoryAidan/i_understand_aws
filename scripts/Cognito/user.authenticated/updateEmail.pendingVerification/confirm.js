require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);
const CognitoAuthTokens		=	AwsCognitoHelper.getJsonWebTokens();

//	Allows you to pass in arguments in the command line.
const yargs					=	require('yargs/yargs');
const {hideBin}				=	require('yargs/helpers');
const argv					=	yargs( hideBin(process.argv) ).argv;

(async ()=>{

	const params = {
		AccessToken		:	CognitoAuthTokens.AccessToken,
		AttributeName	:	argv.attribute || 'email',
		Code			:	String(argv.code),
	};

	//	If AWS Doesn't give us an error when confirming the attribute, we continue with setting .email to the desired email.
	await CISP.verifyUserAttribute(params).promise().then(async function(response){
		console.log('.verifyUserAttribute() response:',response);

		//	The server has kept track of what the 'desired' email address is.
		//	Get a fresh User object so we have the most current data for what the desired email address is.
		await CISP.getUser({
			AccessToken:CognitoAuthTokens.AccessToken
		}).promise().then(async function(User){
			console.log('.getUser() response:',User);


			//	Since we know the code was used to successfully verify the new email address, continue with actually setting the email. It will succeed this time.
			const desired_email	=	User.UserAttributes.find(attr=>attr.Name==='custom:updateEmail:desired').Value;
			await CISP.updateUserAttributes({
				AccessToken		:	CognitoAuthTokens.AccessToken,
				UserAttributes	:	[
					{
						Name	:	'email',
						Value	:	desired_email,
					},
				],
			}).promise()
			.then(function(response){
				console.log('.updateUserAttributes() response:',response);
			})
			.catch(function(error){
				console.log('.updateUserAttributes() error:',error);
			});


		}).catch(error=>console.log(Object.entries(error)));


	}).catch(error=>console.log(Object.entries(error)));


})();

//	CLI Example
//	node ./scripts/Cognito/user.authenticated/updateEmail/confirm.js --code=816723
