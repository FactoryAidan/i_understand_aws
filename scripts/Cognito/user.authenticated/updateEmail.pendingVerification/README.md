# When the Cognito UserPool is configured to:

In Cognito's Console > UserPool > General Settings
- Then **Attributes** > *How do you want your end users to sign in?*:
	- **Email address or phone number** > Uses **Email Address** as `username` to signUp & signIn.
- Then **MFA & verifications** > *Which attributes do you want to verify?*:
	- Is set to `email`

This will make it so when the SDK's `updateUserAttributes()` method is invoked with `email` inside of the request, the user will receive a verification code via email.

# Interesting Facts

-	Changing `email`, confirming `email`, and resending the confirmation code does require authentication in those API calls. So you need to be signed in.
-	Initiating the change doesn't invalidate your `AccessToken`, you can still do all this without reauthing.
-	If your AccessToken expires midway through this process, that's okay. You can signIn with the new email address even if it's still unconfirmed. signIn or use your latest `RefreshToken` to get a new `AccessToken` that lets you finish confirming the `email` update.
-	After changing the `email` user attribute. That email can immediately be used to signIn, even without being confirmed. It's us your your own implementation of Cognito in your app whether you want to restrict users with unconfirmed `email` user attributes.

# Workarounds

-	If you wish not to change the User's `email` attribute immediately upon updating the user attribute, you can use Lambda triggers to intercept the change, un-do it while storing the new `email` as a custom attribute, and then complete the swap in a separate Lambda that watches for the User to successfully confirm the `email` attribute.

# Steps

-	Create a new customer attributes called `updateEmail:current` & `updateEmail:desired`. Cognito will let you access this as `userAttributes['custom:updateEmail:desired']`
-	In the Cognito web-console, update your **App client** click *Set attribute read and write permissions* and a select `custom:updateEmail:desired` access for both read and write.
-	Set a `Custom message` trigger on your Cognito User Pool.
-	FYI: The Lambda must return the `event` object it is originally given.
-	Give you Lambda IAM Role permission to invoke Cognito.adminUpdateUserAttributes();

//-	Make the Lambda set `event.request.userAttributes.email` to the value of `event.request.userAttributes.custom:updateEmail:desired`. This will keep their existing email the same but still deliver the confirmation code to the NEW email.


# Flow

#### Mobile App Initiates the `.email` change which delivers the confirmation code to the new email address.
Attributes Changed:
```
custom:updateEmail:desired		//	Don't touch this
custom:updateEmail:current		👈	Set this to the existing email
email							👈	Set this to the desired email
```

#### Lambda Custom Message Handler - Part 1
>	The Lambda knows that you're asking for a code delivery but not an actual email change when:
```
custom:updateEmail:current !== email
```
Attributes Changed:
```
custom:updateEmail:desired		👈	We set this based on what you sent in `.email` . Just so you can read it later if you need to.
custom:updateEmail:current		//	Don't touch this
email							👈	We set this based on what you sent in `custom:updateEmail:current`. We basically 'revert' your update after delivering the confirmation code to the new email.
```
Gist:
>	We revert the `email` change but save the value for you in `custom:updateEmail:desired`.
At this point, we've '*set the trap*' for the following scenario:
```
custom:updateEmail:desired === email
```


#### User receives email, Mobile App Confirms Code and then Updates email 'again'
Attributes Changed:
```
custom:updateEmail:desired		//	Don't touch this
custom:updateEmail:current		//	Don't touch this
email							👈	Set this 'again' to the `custom:updateEmail:desiredEmail`
```

#### Lambda Custom Message Handler - Part 2
>	The Lambda knows that you've successfully verified the new email with the confirmation-code provided when:
```
custom:updateEmail:desired === email
```
Attributes Changed:
```
custom:updateEmail:desired		👈	We set this to '' to signify that this process is over; so it doesn't get triggered again.
custom:updateEmail:current		👈	We set this to '' to signify that this process is over; so it doesn't get triggered again.
email							//	Don't touch this
```