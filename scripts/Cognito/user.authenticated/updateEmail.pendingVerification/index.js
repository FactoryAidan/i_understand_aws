require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);
const CognitoAuthTokens		=	AwsCognitoHelper.getJsonWebTokens();
const JWT					=	require('jsonwebtoken');

//	Allows you to pass in arguments in the command line.
const yargs					=	require('yargs/yargs');
const {hideBin}				=	require('yargs/helpers');
const argv					=	yargs( hideBin(process.argv) ).argv;

//	This user is authenticated, which means they have an IdToken from Cognito.
//	We want the currently used email from the current user's IdToken.
AuthenticatedUserData	=	JWT.decode(CognitoAuthTokens.IdToken);
console.log(`'custom:verifiedEmail': ${AuthenticatedUserData['custom:verifiedEmail']} 👉 ${AuthenticatedUserData.email}`);
console.log(`'email': ${AuthenticatedUserData.email} 👉 ${argv.new_email}`);

(async ()=>{

	const params = {
		AccessToken		:	CognitoAuthTokens.AccessToken,
		UserAttributes	:	[
			{
				Name	:	'email',
				Value	:	argv.new_email,			//	Desired Email
			},
			{
				Name	:	'custom:updateEmail:current',
				Value	:	AuthenticatedUserData.email,	//	Currently Working Email
			},
		],
	};

	await CISP.updateUserAttributes(params).promise()
		.then(response=>console.log(response))
		.catch(error=>console.log(Object.entries(error)))
		;

})();

//	CLI Example
//	node ./scripts/Cognito/user.authenticated/updateEmail --new_email=Test@Testing.com
