const Dotenv	=	require('dotenv');	Dotenv.config();
//	const fetch		=	require('node-fetch');
global.fetch		=	require('node-fetch');
const fetchIntercept		=	require('fetch-intercept');

//	-------------------------
//	Fetch Intercept Debugging
//	-------------------------
/*
const unregister_intercept	=	fetchIntercept.register({
	
	request: function(url, config){
		const { headers,body }	=	config;
		console.log('HERE ARE YOUR REQUEST HEADERS:',headers);
	//	console.log(JSON.parse(config.body));
		return [url, config];
	},

});
*/

const JWT		=	require('jsonwebtoken');
const FS		=	require('fs');
const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

//	---------------------------------------------------------------------------------
//	Get our saved & current JSON Web Token from normally signIn() to Cognito UserPool
//	---------------------------------------------------------------------------------
const	UsersAuthenticationResult	=	JSON.parse( FS.readFileSync(`${__dirname}/../.users_current_token`,'utf8') );

//	-----------------------------------------------------------------
//	Set User's UserPool JSON Web Token as our IdentityPool credential
//	-----------------------------------------------------------------

//	*	Construct a String representation the Identity Provider
//	These two yield identical results:
//	First is faster but second validates the JSON Web Token expiry.
//	1.)
//	const aws_cognito_identity_providerName	=	`cognito-idp.${process.env.AWS_REGION}.amazonaws.com/${process.env.AWS_COGNITO_USER_POOL_ID}`;
//	2.)
	const Jwt						=	JWT.decode(UsersAuthenticationResult.IdToken);
	const identity_provider_name	=	Jwt.iss.replace(/^https:\/\//,'');	//	aka. Issuer Name



//	Create Logins Mapping of Identity Providers to their access Tokens for this User.
//	Give that Map to the Identity Pool.
const Logins	=	{};
Logins[identity_provider_name]	=	UsersAuthenticationResult.IdToken;


AWS.config.region		=	process.env.AWS_REGION;
AWS.config.credentials	=	new AWS.CognitoIdentityCredentials({
	IdentityPoolId	:	process.env.AWS_COGNITO_IDENTITY_POOL_ID,
	Logins			:	Logins,
});

//	----------------------------------------------------------------------
//	Exchange UserPool IdToken for IdentityPool's temporary IAM credentials
//	----------------------------------------------------------------------
// AWS.config.credentials.get(function(error,data){
// 
// 	if( error )	console.log(error);
// 
// 	console.log({
// 		identityId		:	AWS.config.credentials.identityId,
// 		accessKeyId		:	AWS.config.credentials.accessKeyId,
// 		secretAccessKey	:	AWS.config.credentials.secretAccessKey,
// 		sessionToken	:	AWS.config.credentials.sessionToken,
// 	});
// 
// });

(async ()=>{

	const aws_iam_credentials	=	await AWS.config.credentials.getPromise().then(function(){
		//	Credentials are refreshed on existing Object.
		const le_creds	=	{
			identityId		:	AWS.config.credentials.identityId,
			accessKeyId		:	AWS.config.credentials.accessKeyId,
			secretAccessKey	:	AWS.config.credentials.secretAccessKey,
			sessionToken	:	AWS.config.credentials.sessionToken,
			expired			:	AWS.config.credentials.expired,
			expireTime		:	AWS.config.credentials.expireTime,
		};
	//	console.log(le_creds);
		return le_creds;
	}).catch(error=>console.log(error));

	console.log(aws_iam_credentials);

	//	Manually set (if desired)
	aws_iam_credentials.region			=	'us-west-2';
	aws_iam_credentials.service			=	'iotdata';




//	-----------------------
//	Create Fetch Parameters
//	-----------------------
	const params	=	{
		iot_endpoint	:	'a13j27jomdov5r-ats.iot.us-west-2.amazonaws.com',
		thing_name		:	'Violux_Thing_4',
		shadow_name		:	'',
	};

	const fetch_url		=	`https://${params.iot_endpoint}/things/${params.thing_name}/shadow?name=${params.shadow_name}`;
	const fetch_options	=	{
		method: 'POST',
		body: '{"string":"Bobby"}',
		headers	:	{
		//	Authorization	:	null,
		},
	};
//	fetch_options.headers['X-Amz-Security-Token']	=	aws_iam_credentials.sessionToken;


//	---------------------------
//	Create Authorization Header: AWS SigV4
//	---------------------------
	const ASV4		=	require('awssignaturev4');
	const ASVFour	=	ASV4.fromFetch(fetch_url,fetch_options,aws_iam_credentials)
	//	.withAwsIamCredentials(iam_credentials)
//		.withTestData(0)
		.attachAuthorizationHeaderToFetchOptions()
//		.generateAuthorizationHeaderValue()
		;
//	console.log(tmp);
//	return;	//	Debugging Only


//	-----------
//	Test
//	-----------
	const v4 = require('aws-signature-v4');


//	-----------
//	Test | PresignedUrl
//	-----------
	if( false ){
		const PresignedUrl	=	v4.createPresignedURL(
			ASVFour.Params.http.method,
			ASVFour.Params.location.host,
			ASVFour.Params.location.pathname,
			'iot',
			ASVFour.Params.http.body,
			{
				key		:	ASVFour.Params.aws.accessKeyId,
				secret	:	ASVFour.Params.aws.secretAccessKey,
				headers	:	ASVFour.Params.http.headers,
				query	:	ASVFour.Params.location.search,
			}
		);
		console.log('👇👇👇👇 COMPARISON PresignedUrl 👇👇👇👇');
		console.log(PresignedUrl);
		console.log('👆👆👆👆 COMPARISON PresignedUrl 👆👆👆👆');

	//	-----------
	//	Test | CanonicalRequest
	//	-----------
		const CanonicalRequest	=	v4.createCanonicalRequest(
			ASVFour.Params.http.method,
			ASVFour.Params.location.pathname,
			ASVFour.Params.location.search,
			ASVFour.Params.http.headers,
			ASVFour.Params.http.body
		);
		console.log('👇👇👇👇 COMPARISON CanonicalRequest 👇👇👇👇');
		console.log(CanonicalRequest);
		console.log('👆👆👆👆 COMPARISON CanonicalRequest 👆👆👆👆');
	}//if

//	-----
//	Fetch
//	-----
//	fetch_options.headers['x-amz-security-token']	=	aws_iam_credentials.sessionToken;

//	For IoT.. this is not signed into the request. It is added as a header after.
//	Although, it looks to behave well even if it is signed into the request.
	fetch_options.headers['X-Amz-Security-Token']	=	aws_iam_credentials.sessionToken;

//	console.log(fetch_url,fetch_options); 
	fetch_options.body	=	'{"string":"Bobby1"}';
	await fetch(fetch_url,fetch_options)
	.then(response=>{
		console.log(
		//	response,
			response.status,
			response.statusText,
			response.headers
		);
		return response.json();
	})
	.then(response=>{
		console.log(response);
	})
	.catch(error=>console.log(error));

})();
