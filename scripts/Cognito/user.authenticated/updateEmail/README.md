# When the Cognito UserPool is configured to:

In Cognito's Console > UserPool > General Settings
- Then **Attributes** > *How do you want your end users to sign in?*:
	- **Email address or phone number** > Uses **Email Address** as `username` to signUp & signIn.
- Then **MFA & verifications** > *Which attributes do you want to verify?*:
	- Is set to `email`

This will make it so when the SDK's `updateUserAttributes()` method is invoked with `email` inside of the request, the user will receive a verification code via email.

# Interesting Facts

-	Changing `email`, confirming `email`, and resending the confirmation code does require authentication in those API calls. So you need to be signed in.
-	Initiating the change doesn't invalidate your `AccessToken`, you can still do all this without reauthing.
-	If your AccessToken expires midway through this process, that's okay. You can signIn with the new email address even if it's still unconfirmed. signIn or use your latest `RefreshToken` to get a new `AccessToken` that lets you finish confirming the `email` update.
-	After changing the `email` user attribute. That email can immediately be used to signIn, even without being confirmed. It's us your your own implementation of Cognito in your app whether you want to restrict users with unconfirmed `email` user attributes.

# Workarounds

-	If you wish not to change the User's `email` attribute immediately upon updating the user attribute, you can use Lambda triggers to intercept the change, un-do it while storing the new `email` as a custom attribute, and then complete the swap in a separate Lambda that watches for the User to successfully confirm the `email` attribute.

# Steps

-	Create a new customer attribute called `verifiedEmail`. Cognito will let you access this as `userAttributes['custom:verifiedEmail']`
-	In the Cognito web-console, update your **App client** click *Set attribute read and write permissions* and a select `custom:verifiedEmail` access for both read and write.
-	Set a `Custom message` trigger on your Cognito User Pool.
-	FYI: The Lambda must return the `event` object it is originally given.
-	Make the Lambda set `event.request.userAttributes.email` to the value of `event.request.userAttributes.custom:verifiedEmail`. This will keep their existing email the same but still deliver the confirmation code to the NEW email.
-	Give you Lambda IAM Role permission to invoke Cognito.adminUpdateUserAttributes();
