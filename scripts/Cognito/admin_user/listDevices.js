require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);
const CognitoAuthTokens		=	AwsCognitoHelper.getJsonWebTokens();

(async ()=>{

	const function_params = {
		AccessToken		:	CognitoAuthTokens.AccessToken,
	//	Limit			:	null	//	'NUMBER_VALUE',
	//	PaginationToken	:	''		//	'STRING_VALUE',
	};

	const response	=	await CISP.listDevices(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
