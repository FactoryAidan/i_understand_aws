require('dotenv').config();
const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

(async ()=>{

	const search_email	=	process.env.A_TEST_EMAIL_ADDRESS;

	const function_params = {
		UserPoolId		:	process.env.AWS_COGNITO_USER_POOL_ID,
		AttributesToGet	:	[
			'email',
		],
		Filter			:	`email = "${search_email}"`,
		Limit			:	'10',
	//	PaginationToken	:	'STRING_VALUE'
	};

	const response	=	await CISP.listUsers(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
