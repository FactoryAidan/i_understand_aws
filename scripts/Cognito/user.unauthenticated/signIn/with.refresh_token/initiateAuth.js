const {AwsCognitoHelper}	=	require(`${process.cwd()}/AwsHelpers.js`);
const Dotenv	=	require('dotenv');	Dotenv.config();
const FS		=	require('fs');
const JWT		=	require('jsonwebtoken');
const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

const	UsersAuthenticationResult	=	JSON.parse( FS.readFileSync(`${process.cwd()}/scripts/Cognito/user.with_access_token/.users_current_token`,'utf8') );
const	Jwt							=	JWT.decode(UsersAuthenticationResult.AccessToken);

const params = {
	USERNAME		:	Jwt.username,	//	Use the username defined in the JSON Web Token because it might be an internal identifier instead of what you think it is.
	REFRESH_TOKEN	:	UsersAuthenticationResult.RefreshToken,
};

(async ()=>{

	const function_params = {
	//	AuthFlow		:	'REFRESH_TOKEN_AUTH',
		AuthFlow		:	'REFRESH_TOKEN',
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		AuthParameters	:	{
			REFRESH_TOKEN	:	params.REFRESH_TOKEN,
			SECRET_HASH	:	AwsCognitoHelper.calculateSecretHash({
				user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
				user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
				user_pool_user_name			:	params.USERNAME,
			}),
		},
	};

	const response	=	await CISP.initiateAuth(function_params).promise()
		.then( response =>{
			//	Note: this response will not contain a RefreshToken; it only has AccessToken and IdToken.
			//	You'll keep the existing RefreshToken until you do a full re-authentication using the signIn process.
			console.log('↙️ initiateAuth() response:',response);
			AwsCognitoHelper.saveJsonWebTokens(undefined,response.AuthenticationResult);
		})
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
