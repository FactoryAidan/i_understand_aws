const {AwsCognitoHelper}	=	require(`${process.cwd()}/AwsHelpers.js`);
const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

const params = {
	USERNAME	:	process.env.A_TEST_USERNAME,
//	PASSWORD	:	'A_fake_user_02',
	PASSWORD	:	'My_new_password_01',
};

(async ()=>{

	const function_params = {
		AuthFlow		:	'USER_PASSWORD_AUTH',
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		AuthParameters	:	{
			USERNAME	:	params.USERNAME,
			PASSWORD	:	params.PASSWORD,
			SECRET_HASH	:	AwsCognitoHelper.calculateSecretHash({
				user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
				user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
				user_pool_user_name			:	params.USERNAME,
			}),
		},
	};

	const response	=	await CISP.initiateAuth(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
