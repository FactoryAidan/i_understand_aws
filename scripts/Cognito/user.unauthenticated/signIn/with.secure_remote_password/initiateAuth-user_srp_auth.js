//	References
//	-	https://medium.com/swlh/what-is-secure-remote-password-srp-protocol-and-how-to-use-it-70e415b94a76
//	-	https://github.com/mozilla/node-srp

const {AwsCognitoHelper}	=	require(`${process.cwd()}/AwsHelpers.js`);
const Dotenv				=	require('dotenv');	Dotenv.config();

const BigInteger			=	require('./helpers/BinaryBigInteger.js');
const AuthenticationHelper	=	require('./helpers/AuthenticationHelper.js');

const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();

const sjcl					=	require('sjcl-aws');

const params	=	{
	PASSWORD		:	'My_new_password_01',
	user_pool_name	:	process.env.AWS_COGNITO_USER_POOL_ID.split('_')[1],
};

const AH					=	new AuthenticationHelper(params.user_pool_name);

//	----------------
//	Generate Large A
//	----------------
AH.getLargeAValue(function(err,A){
//	params.SRP_A_RAW	=	A;
	params.SRP_A		=	AH.padHex(A);
});
console.log('SRP_A:',params.SRP_A);
//	return;






const { SRPClient, calculateSignature, getNowString }	=	require('amazon-user-pool-srp-client');
const SRP	=	new SRPClient(params.user_pool_name);
//	console.log('SRP_A:',SRP.calculateA());
	params.SRP_A	=	SRP.calculateA();
//	return;




(async ()=>{

	//	----------
	//	Send SRP_A
	//	----------
	let tmp_params = {
		AuthFlow		:	'USER_SRP_AUTH',
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		AuthParameters	:	{
			USERNAME	:	process.env.A_TEST_USERNAME,
			SRP_A		:	params.SRP_A,
			SECRET_HASH	:	AwsCognitoHelper.calculateSecretHash({
				user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
				user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
				user_pool_user_name			:	process.env.A_TEST_USERNAME,
			}),
		},
	};

	console.log('↗️ initiateAuth() with:',tmp_params);
	await CISP.initiateAuth(tmp_params).promise()
		.then(({ ChallengeName,ChallengeParameters,Session})=>{

			console.log('↙️ initiateAuth() response:');
			console.log(ChallengeName,ChallengeParameters,Session);


			//	----
			//	HKDF - TEST
			//	----
			/*
			const hkdf_test		=	SRP.getPasswordAuthenticationKey(
				ChallengeParameters.USER_ID_FOR_SRP,
				params.PASSWORD,
				ChallengeParameters.SRP_B,
				ChallengeParameters.SALT
			);
			console.log('hkdf_test:');
			console.log( hkdf_test );
			//	hkdf_test:	[ 289733671, 670039371, -1566500566, -1697812234 ]
			return;
*/
			const test_value	=	Buffer.from(new Int8Array([289733671,670039371,-1566500566,-1697812234 ]));

			console.log(test_value.toString('base64'));

			//	----
			//	HKDF
			//	----
			let hkdfValue;
			AH.getPasswordAuthenticationKey(
				ChallengeParameters.USER_ID_FOR_SRP,
				params.PASSWORD,
				new BigInteger(ChallengeParameters.SRP_B,16),
				new BigInteger(ChallengeParameters.SALT,16),
				function(err,given_hkdfValue){hkdfValue=given_hkdfValue;}
			);
			hkdfValue	=	hkdfValue.toString('base64');
			console.log('hkdfValue:',hkdfValue);
			//	hkdfValue: 3MvVWGaSEJWPYUuBZ2k6Fg==
			return;	//	Debugging Only

			//	----
			//	Date
			//	----
			const dateNow	=	formatDate(new Date);

			//	---------
			//	Signature
			//	---------

			const signatureString	=	calculateSignature(
				hkdfValue,
				params.user_pool_name,
				ChallengeParameters.USER_ID_FOR_SRP,
				ChallengeParameters.SECRET_BLOCK,
				dateNow
			);
			console.log(signatureString);

		//	--------------------
		//	Respond to Challenge
		//	--------------------
			tmp_params = {
				ChallengeName		:	'PASSWORD_VERIFIER',
				ClientId			:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
				ChallengeResponses	:	{
					USERNAME					:	ChallengeParameters.USER_ID_FOR_SRP,
					PASSWORD_CLAIM_SIGNATURE	:	signatureString,
					PASSWORD_CLAIM_SECRET_BLOCK	:	ChallengeParameters.SECRET_BLOCK,
					TIMESTAMP					:	dateNow,
					SECRET_HASH	:	AwsCognitoHelper.calculateSecretHash({
						user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
						user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
						user_pool_user_name			:	ChallengeParameters.USER_ID_FOR_SRP,
					}),
				},
				Session				:	Session,
			};
			console.log('↗️ respondToAuthChallenge() with:',tmp_params);
			return CISP.respondToAuthChallenge(tmp_params).promise();
		})
		.then( response =>{
			console.log('↙️ respondToAuthChallenge() response:',response);
		//	console.log(AuthenticationResult);
		//	return {username: ChallengeParameters.USERNAME, credentials: AuthenticationResult };
		})
		.catch(error=>{
			console.log('ERROR:',error);
		//	console.log('ERROR:',Object.entries(error));
		})
		;

})();//async


function formatDate(given_date){
	const parts	=	given_date.toString().split(' ');
	return [
		parts[0],	//	Dayname
		parts[1],	//	Month
		parts[2],	//	Day
		parts[4],	//	Time
		parts[5].split('-')[0],	//	Zone
		parts[3],	//	Year
	].join(' ');
}
/*
function calculateSignature (hkdf, userPoolId, username, secretBlock, dateNow) {
  var mac = new sjcl.misc.hmac(hkdf)
  mac.update(sjcl.codec.utf8String.toBits(userPoolId))
  mac.update(sjcl.codec.utf8String.toBits(username))
  mac.update(sjcl.codec.base64.toBits(secretBlock))
  mac.update(sjcl.codec.utf8String.toBits(dateNow))
  return sjcl.codec.base64.fromBits(mac.digest())
}
*/