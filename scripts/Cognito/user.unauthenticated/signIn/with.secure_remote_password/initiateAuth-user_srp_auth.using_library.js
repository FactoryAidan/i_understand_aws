require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);

//	Allows you to pass in arguments in the command line.
// const yargs					=	require('yargs/yargs');
// const {hideBin}				=	require('yargs/helpers');
// const argv					=	yargs( hideBin(process.argv) ).argv;

const { SRPClient, calculateSignature, getNowString }	=	require('amazon-user-pool-srp-client');

const params	=	{
	//	AWS expects just 'part' of the UserPool.id listed in AWS's console.
	//	us-west-2_Wy4x8UHWy	<- given this, we want to ignore the region and just use the trailing base64 characters.
	user_pool_name	:	process.env.AWS_COGNITO_USER_POOL_ID.split('_')[1],
};

const SRP		=	new SRPClient(params.user_pool_name);

(async ()=>{

	//	----------
	//	Send SRP_A
	//	----------
	let tmp_params = {
		AuthFlow		:	'USER_SRP_AUTH',
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		AuthParameters	:	{
			USERNAME	:	process.env.A_TEST_USERNAME,
			SRP_A		:	SRP.calculateA(),
			SECRET_HASH	:	AwsCognitoHelper.calculateSecretHash({
				user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
				user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
				user_pool_user_name			:	process.env.A_TEST_USERNAME,
			}),
		},
	};

//	console.log('↗️ initiateAuth() with:',tmp_params);
	await CISP.initiateAuth(tmp_params).promise()
		.then(({ ChallengeName,ChallengeParameters,Session})=>{

		//	console.log('↙️ initiateAuth() response:');
		//	console.log(ChallengeName,ChallengeParameters,Session);

			const hkdf		=	SRP.getPasswordAuthenticationKey(
				ChallengeParameters.USER_ID_FOR_SRP,
				process.env.A_TEST_PASSWORD,
				ChallengeParameters.SRP_B,
				ChallengeParameters.SALT
			);
			const dateNow	=	getNowString();
			const signatureString = calculateSignature(
				hkdf,
				params.user_pool_name,
				ChallengeParameters.USER_ID_FOR_SRP,
				ChallengeParameters.SECRET_BLOCK,
				dateNow
			);

		//	--------------------
		//	Respond to Challenge
		//	--------------------
			tmp_params = {
				ChallengeName		:	'PASSWORD_VERIFIER',
				ClientId			:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
				ChallengeResponses	:	{
					USERNAME					:	ChallengeParameters.USER_ID_FOR_SRP,
					PASSWORD_CLAIM_SIGNATURE	:	signatureString,
					PASSWORD_CLAIM_SECRET_BLOCK	:	ChallengeParameters.SECRET_BLOCK,
					TIMESTAMP					:	dateNow,
					SECRET_HASH	:	AwsCognitoHelper.calculateSecretHash({
						user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
						user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
						user_pool_user_name			:	ChallengeParameters.USER_ID_FOR_SRP,
					}),
				},
				Session				:	Session,
			};
		//	console.log('↗️ respondToAuthChallenge() with:',tmp_params);
			return CISP.respondToAuthChallenge(tmp_params).promise();
		})
		.then( response =>{
			console.log('↙️ respondToAuthChallenge() response:',response);
			AwsCognitoHelper.saveJsonWebTokens(response.AuthenticationResult,undefined);
		//	console.log(AuthenticationResult);
		//	return {username: ChallengeParameters.USERNAME, credentials: AuthenticationResult };
		})
	//	.catch(error=>console.log('ERROR:',Object.entries(error)))
		.catch(error=>console.log('ERROR:',error))
		;

})();//async