const {AwsCognitoHelper}	=	require(`${process.cwd()}/AwsHelpers.js`);
const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

const params	=	{
	ConfirmationCode:	'770707',	// {String}	This comes from the confirmation email sent to the email address provided to  'CISP.signUp()'
	Username		:	process.env.A_TEST_USERNAME,	//	Depending on your User Pool settings, this could be an email address used as the username.
};

(async ()=>{

	const function_params = {
		ConfirmationCode:	params.ConfirmationCode,
		Username		:	params.Username,
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		SecretHash		:	AwsCognitoHelper.calculateSecretHash({
			user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
			user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
			user_pool_user_name			:	params.Username,
		}),
	};

	await CISP.confirmSignUp(function_params).promise()
		.then(response=>console.log(response))
		.catch(error=>console.log(Object.entries(error)))
		;

})();
