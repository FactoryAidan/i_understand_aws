require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);

const params	=	{
	Username		:	process.env.A_TEST_USERNAME,	//	Depending on your User Pool settings, this could be an email address used as the username.
};

(async ()=>{

	const function_params = {
		Username		:	params.Username,
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		SecretHash		:	AwsCognitoHelper.calculateSecretHash({
			user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
			user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
			user_pool_user_name			:	params.Username,
		}),
	};

	await CISP.resendConfirmationCode(function_params).promise()
		.then(response=>{
			console.log("Upon success, this endpoint simply returns '{}'. That's all.");
			console.log("Here's the response:",response);
		})
		.catch(error=>console.log(Object.entries(error)))
		;

})();
