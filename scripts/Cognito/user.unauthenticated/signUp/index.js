require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);
const CognitoAuthTokens		=	AwsCognitoHelper.getJsonWebTokens();

const params	=	{
	Username	:	process.env.A_TEST_USERNAME,
	Password	:	process.env.A_TEST_PASSWORD,
	email		:	process.env.A_TEST_EMAIL_ADDRESS,
};

(async ()=>{

	const tmp_params = {
		ClientId	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		Password	:	params.Password,
		Username	:	params.Username,
		UserAttributes: [
			{
				Name	:	'email',
				Value	:	params.email,
			},
			{
				Name	:	'nickname',
				Value	:	'test_value',
			},
		],
		SecretHash		:	AwsCognitoHelper.calculateSecretHash({
			user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
			user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
			user_pool_user_name			:	params.Username,
		}),
	};

	const response	=	await CISP.signUp(tmp_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
