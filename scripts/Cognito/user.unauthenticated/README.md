# `USER_PASSWORD_AUTH` flow

- `signUp.js` | Creates an unconfirmed user and delivers a confirmation code to the email address used to sign-up
- `confirmSignUp.js` | Changes the user's status to 'confirmed'; the user can now use their account normally.
- `initiateAuth.js` | With a username & password, the user signs in and receives tokens they can use to access whatever you want to let them access.

# `USER_SRP_AUTH` flow

*Secure Remote Password* (SRP)

- `signUp.js` | Creates an unconfirmed user and delivers a confirmation code to the email address used to sign-up
- `confirmSignUp.js` | Changes the user's status to 'confirmed'; the user can now use their account normally.
- `initiateAuth.js` | With a username & password, the user signs in and receives tokens they can use to access whatever you want to let them access.


# User `RESET_REQUIRED` Account status

When you click *Reset password* on the User in Cognito's web-admin, it puts the account status into `RESET_REQUIRED`.

When you click that button, Cognito also sends a confirmationCode via email to the User's verified email address. However, that code doesn't work when using confirmForgotPassword() and you ultimately have to get a second confirmationCode with forgotPassword().

Basically, it's a crummy user-experience right now. But At least you know now.