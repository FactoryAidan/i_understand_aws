//	------
//	If you click the 'reset' button on a User in AWS Cognito, it forces them to do this process
//	------
const {AwsCognitoHelper}	=	require(`${process.cwd()}/AwsHelpers.js`);
const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');
const CISP		=	new AWS.CognitoIdentityServiceProvider();

const params = {
	USERNAME	:	process.env.A_TEST_EMAIL_ADDRESS,
	PASSWORD	:	'A_fake_user_02',
};

(async ()=>{

	const function_params = {
		ChallengeName	:	'NEW_PASSWORD_REQUIRED',
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		Session			:	'this_is_my_session_identifier',
		ChallengeResponses	:	{
			USERNAME		:	params.USERNAME,
			NEW_PASSWORD	:	params.PASSWORD,
			SECRET_HASH		:	AwsCognitoHelper.calculateSecretHash({
				user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
				user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
				user_pool_user_name			:	params.USERNAME,
			}),
		},
	};

	const response	=	await CISP.respondToAuthChallenge(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
