require('dotenv').config();
const AWS					=	require('aws-sdk');
const CISP					=	new AWS.CognitoIdentityServiceProvider();
const {AwsCognitoHelper}	=	require(`${process.cwd()}/helpers/AWS`);

const params	=	{
	Password		:	process.env.A_TEST_PASSWORD,
	Username		:	process.env.A_TEST_USERNAME,	//	Depending on your User Pool settings, this could be an email address used as the username.
	ConfirmationCode:	'707563',
};

(async ()=>{

	const function_params = {
		ConfirmationCode:	params.ConfirmationCode,
		ClientId		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
		Password		:	params.Password,
		Username		:	params.Username,
		SecretHash		:	AwsCognitoHelper.calculateSecretHash({
			user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
			user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
			user_pool_user_name			:	params.Username,
		}),
	};

	const response	=	await CISP.confirmForgotPassword(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log(response);

})();
