const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();


let response;
let function_params;
(async ()=>{


	function_params = {
	};

	response	=	await IOT.listThings(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('THINGS:',response);





	function_params = {
	//	ascendingOrder	:	true || false,
	//	marker			:	'STRING_VALUE',
	//	pageSize		:	'NUMBER_VALUE',
	};

	response	=	await IOT.listCertificates(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('CERTIFICATES:',response);

})();
