const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

const params = {
	caCertificateId	:	'95bb283689457459e7309fdd3dfe53beb275d5c188614bfc92b363fcbb5f2192',
//	ascendingOrder	:	true || false,
//	marker			:	'STRING_VALUE',
//	pageSize		:	'NUMBER_VALUE'
};


(async ()=>{

	const response	=	await IOT.listCertificatesByCA(params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('CERTIFICATES by CA:',response);

})();
