const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

(async ()=>{

	const function_params = {
		certificateId	:	'f41ee12f733b7b4a33515c4d1ea3abf94ffc3a41d2d428f58b9cac0b3bbd8aaa',
	};

	const response	=	await IOT.describeCertificate(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('CERTIFICATE:',response);

})();
