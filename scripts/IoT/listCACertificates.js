const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

(async ()=>{

	const function_params = {
	//	ascendingOrder	:	true || false,
	//	marker			:	'STRING_VALUE',
	//	pageSize		:	'NUMBER_VALUE',
	};

	const response	=	await IOT.listCACertificates(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('CA CERTIFICATES:',response);

})();
