const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();


let response;
let function_params;
(async ()=>{


	function_params = {
	//	target			:	'us-west-2:e12b2f4f-aa98-4705-aeed-527b934e7862',	//	Cognito Identity ID	| Aidan
		target			:	'us-west-2:36f11e36-f2bb-45de-9ded-ece69551a623',	//	Cognito Identity ID | Matti
	//	marker			:	'STRING_VALUE',
	//	pageSize		:	'NUMBER_VALUE',
		recursive		:	true
	};

	response	=	await IOT.listAttachedPolicies(function_params).promise()
	//	.catch(error=>console.log(Object.entries(error)))
		.catch(error=>console.log(error))
		;

	console.log('POLICIES:',response);

})();
