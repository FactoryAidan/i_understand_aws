const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

const params = {
	certificate_id	:	'15ec11f47740e53e76ffada937a46ab7c3abbe3d084bf42a1d0889e3891feafb',
};


let response;
let function_params;
(async ()=>{

	function_params = {
		certificateId	:	params.certificate_id,
		forceDelete		:	true,
	};

	response	=	await IOT.deleteCertificate(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('CERTIFICATES:',response);

})();
