# Here's how!

#### Topic:

> Connect to **IoT** using **Temporary IAM credentials** traded from the `IdToken` of an **Authenticated Cognito User** with an **Identity** inside of a **Cognito IdentityPool**.

We're going to step backwards here from where you are and move to where you *should* have started.

*But don't feel bad.
Nobody knows where to start because AWS doesn't document how their parts fit together, they just document individual parts without context and leave it up to you to piece it together.*

# 🎓Academic Reading

#### 🔵Goal: Connect to IoT Cloud

There are two ways to connect to MQTT:

- MQTT over TLS (Using **PKI Certificates**)
- MQTT over websockets over HTTPS (Using 👉 **AWS Signature V4**)

> Note: We're not talking about Authenticating with **PKI Certificates** for this.

#### 🔵Authenticating with **AWS Signature V4**

At the very core of an **AWS Signature V4** are 👉 **IAM credentials**.


#### 🔵IAM Credentials

You must know there are two *flavors* of IAM credentials:

- **Permanent** IAM Credentials
- 👉 **Temporary** IAM Credentials

> FYI: Both IAM Credentials types have the same parts:
> - ACCESS_KEY_ID
> - SECRET_ACCESS_KEY
> - SESSION_TOKEN

#### 🔵Temporary IAM Credentials

**Temporary IAM Credentials** are issued by AWS STS (Secure Token Service).
> *STS is a service that you don't directly interact with but are interacting with basically all the time as you use AWS' suite of services.*

These Temporary IAM Credentials are received after *trading* a user's 👉 AWS `IdToken` from a AWS *Cognito User Pool**.


#### 🔵`IdToken` from Cognito UserPool

You get an `IdToken` from a Cognito UserPool configured with an 👉 **App Client**.

#### 🔵App Client

👉 Build a god-damn website.

> Your **App Client** is what AWS refers to as any of your 'apps' that allows users to signUp/signIn.
> An **App Client** can be a website, iPhone app, some API you built that keeps track of users so they can have an account inside of your App. Do you get it?
> When a user successfully conducts a signUp & signIn using your **App Client**, AWS Cognito provides that user with an `IdToken`.

#### 🔵Wow

Holy shit... we're at the beginning. The process of connecting to AWS IoT starts with a bloody `IdToken`.

Now let's go through all this, in-detail, from the beginning to end.

# 🗂️The Setup Process (outlined)

- Cognito
	- Create UserPool
	- Create UserPool App Client
	- Create IdentityPool
- Write Code using the `aws-sdk`
	- That lets Cognito UserPool Users authenticate with the initiateAuth() method to receive an `IdToken`.
	- Using that User's `IdToken`, initialize a new `AWS.CognitoIdentityCredentials()` object using the User's `IdToken`. Use that object's get() method to return **Temporary IAM Credentials**.
- Lambda
	- That listens for a new Cognito IdentityPool Identity to be created and then..
		- Creates an IoT policy that allows a Cognito IdentityPool Identity to communicate with a Thing.
		- Uses the `aws-sdk`'s `IoT.attachPolicy()` method to attach your **IoT Policy** to your **Cognito IdentityPool Identity**.
- IoT Shadow REST API
	- Write some badass vanilla code or use some `npm` package that:
		- Generates an `Authentication: <signature>` header based on the Temporary IAM Credentials' `accessKeyId` & `secretAccessKey`.
		- Attach that header to your HTTPS requests to the IoT Shadow REST API.
- IoT
	- Use `aws-sdk`'s IoT.attachPolicy() method to attach a policy to the Cognito IdentityPool Identity that is aligned with the user
- That's it. You did it.


# 🗂️The Setup Process (specifics)

# 🟢Accessing AWS Services Using an Identity Pool After Sign-in

Here's a very good (simple) graphic about the overall flow of the whole process.
and sample code about trading your Cognito UserPool `IdToken` for IdentityPool IAM Credentials.

[Accessing AWS Services Using an Identity Pool After Sign-in](https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-integrating-user-pools-with-identity-pools.html)

# 🟢IoT.attachPolicy()

[IoT.attachPolicy() on AWS Docs](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Iot.html#attachPolicy-property)

Create a Policy inside IoT's Console with the following document:

###### `MobileDeviceApplication_IoT_Policy`
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "iot:Connect",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "iot:Publish",
        "iot:Subscribe",
        "iot:Receive"
      ],
      "Resource": "*"
    }
  ]
}
```

Attach this policy using `IoT.attachPolicy()` to the Cognito IdentityPool Identity that was assigned by AWS to your UserPool User.
This method accepts two parameters:
```
policyName	{String}	The name of the policy to attach.
target		{String}	The identity to which the policy is attached.
```

###### 🛑Academic Reading

To consume the [IoT Device Shadow REST API](https://docs.aws.amazon.com/iot/latest/developerguide/device-shadow-rest-api.html) API, you need to authenticate the connection somehow.

For a connection to be authenticated on a MQTT over Web-Sockets request with the AWS Signature V4 header, there needs to be two policies in place:

- The IdentityPool's **Authorized Role** needs a policy allowing communication with IoT.
- The specific user's IdentityPool `Identity` needs to be attached to an IoT `Policy` that allows communication with IoT.

[About Cognito's IAM Policy + IoT Policy](https://docs.aws.amazon.com/iot/latest/developerguide/cognito-identities.html)
[Deeper about Cognito's IAM Policy + IoT Policy](https://docs.aws.amazon.com/iot/latest/developerguide/pub-sub-policy.html#pub-sub-policy-cognito)

[About defining a Lambda to `AttachPolicy()`](https://docs.aws.amazon.com/iot/latest/developerguide/cog-iot-policies.html)
[Code sample for allowing IdentityPool Authenticated role user's to attach a global IoT Policy to their Identity](https://aws.amazon.com/blogs/iot/configuring-cognito-user-pools-to-communicate-with-aws-iot-core/)

If you get responses from the API related to authentication tokens or signatures like the following, you're not even getting through to the Device Shadow REST API (let alone to the IoT policy) and need to do more work on AWS Signature V4 to authenticate your HTTP request.
```
403 Forbidden
{
  message: 'The security token included in the request is invalid.',
  traceId: '7fff6e97-663a-236c-3bfe-53d270213424'
}
{
  message: 'Missing authentication',
  traceId: '805f19b0-811a-fc2e-bc42-ee0baf12b14e'
}
```

If you get a null `message` on a `HTTP 403 Forbidden` response like the following, it indicates you need to use `IoT.attachPolicy()` to attach your Cognito Identity to the IoT Policy.

```
403 Forbidden
{ message: null, traceId: 'a48f9f63-6580-dd44-a798-562c3f98b65b' }
```

Unfortunately, I have not found a way to auto-attach (with a Lambda or such) the IoT policy to the IdentityPool Identity.

I know there are events on the Cognito UserPool for `post-authentication` but the IdentityPool's Temporary IAM Credentials are not generated & issued at that time.
Those IAM credentials are only issued when the user's `Credentials Object` is created from the AWS SDK (not the same as token credentials).
When that occurs, there seems to be absolutely no event to hook a Lambda into.

So to get that `IoT Policy` to be attached to the user's `Identity`, it has to be done manually.
Some ideas are:

- Give the IdentityPool's **Authorized Role** permissions to allow a user to invoke `IoT.attachPolicy()` or create an API endpoint the user can consume with a typical HTTP POST request. So they can attach the Policy to their Identity themselves when they exchange their `IdToken` for Temporary IAM Credentials.
