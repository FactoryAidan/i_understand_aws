const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

const params = {
	certificateId	:	'ddd760c3b3699a1025ae6fe783c03db7ac86a92e0e2f663ca07008ae665e05d6',
	newStatus		:	'ACTIVE',
};

(async ()=>{

	const response	=	await IOT.updateCertificate(params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('CERTIFICATES:',response);

})();
