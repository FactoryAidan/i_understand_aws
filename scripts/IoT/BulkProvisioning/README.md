# You want to Bulk Register IoT Things & Policies

Follow this [AWS article](https://docs.aws.amazon.com/iot/latest/developerguide/bulk-provisioning.html) for initial setup of S3 Bucket and Input File.

#### Helpful Hints:

- You have to pre-create any *Thing.Types* & *Thing.Groups* you define in the input file you upload to **S3**.

# Let AWS IoT assume an IAM role that grants access to S3

If you encounter the following error message the you need to update the IAM role you defined in this method's payload so a *Trusted Entity* is `iot.amazonaws.com`:

> Cannot assume provided role

Follow [this AWS article](https://docs.aws.amazon.com/iot/latest/developerguide/iot-create-role.html).

The article uses the command line equivalent of:

1. Go to IAM Console and create a new role called `IoT_BulkProvisioning_InputSourceS3`
1. Attach the existing **AWS Managed Policy** names of `AmazonS3FullAccess` & `AWSIoTFullAccess`.
1. Edit the **Trust Relationships** to contain:
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "iot.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```
4. You're done. This script can now invoke `startThingRegistrationTask()` and IoT will have permission to access your 'stuff' in **S3**.

# Additional Troubleshooting

If you encounter the following error message the you need to update the **S3** Bucket to allow access:

>  S3 file not accessible using provided role

# Downloading Errors | Results from your ThingRegistrationTask

The `listThingRegistrationTaskReports()` method will provide you a URL in its `resourceLinks[]` JSON property.
Use that link to download the results:
```
curl \
  --output "iot_bulk_registration_report.json" \
  --url "https://aws-iot-btp-prod-us-west-2.s3.us-west-2.amazonaws.com/766363384878/4898b735-9241-462e-95e5-dce28d2a473d/failed/4898b735-9241-462e-95e5-dce28d2a473d-1?X-Amz-Security-Token=IQoJc3JpZ5luX2ZjEAgaCXVzLXdlc3QtNiJHMEUCIQd01onS5L0HoEls2gDBnvn%2B1ICVuRC%2B1qre8Xrrnxq3kAIgfbzEE8r6V9JyhUwUnsGbdSVRF7U%2FbCbSVoQDTgDNa74q0gIIcRBCGgnnNqU2MDc1MTM0ODYiDNOh9BbruKeh2m6pBSqvArF0N8pgmlpcZ8sJTC88qktZMvXEHDkqapjqHxnBzR9U7vBZ6dVjPOqCAZ%2BO%2BzDXCLC07rPxOsRNBxIf%2B3DXAzOZV9XLImWEOXc%2B9h%2FYoGtbM2DR%2BalmYnj667e6yzGL9XxzQqIj%2Ftg0LJtscyd1fjcoyMQ2HeKR7OdknflCK067FwGV22aElb%2FHPsU6eZhqk6jKswqk%2FgFEvw6u%2BYAAErf8Kk5q2yzy1fcJ3aix%2FA7wl0AfkEbuOJaU%2FMun97I%2FMrAqev5SOGp3YVoeXfp1N8QWH0FuCe4nvNkppOoVeDOXbEJlbT%2FvpPsfovmQ4ZTvK%2BGHtwAjkDwhmw1m8yKttYNl2%2Fk%2FZbxlmYhI352qOwNbRSNxZnP34Ht97jUM5G9vnDKMV2jVqcKsaOX7983PMjCLgKn9BTq%2FASF35Axs%2Fk0pyikO0LdmzNj98d7bmeuq4lPVGqgbIJZfC6QEeYw5y5MG05HfeXv0C3fDgvXynsmtbAb%2BJJb8sL8Nz1zSe4GyKFR%9FAhibG31Z%2BeLC1coo%2B6JhWxNLeO43tkTFHuDU450xXe6GLukOmElZqXghUmYh4sOnQkypHz1IWjVJfYYnW5NRDbcQUEmhhG%2B73wJWUEEvQjKSKRBqoNHjfneUnsubvNexAs%2B%2BsMhjhLZN76do1cpCArKyKuKd&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20201110T072355Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=ASIAFARKEUCRGHED4YUR%2F20201110%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=85f6f7d420a19a1e291a21f8f9c6c8aabcc917b9e56809b0b92324ec39285cf5" \
  ;
```

Or this to merely display the results:

```
curl --url "https://aws-iot-btp-prod-us-west-2.s3.us-west-2.amazonaws.com/766363384878/4898b735-9241-462e-95e5-dce28d2a473d/failed/4898b735-9241-462e-95e5-dce28d2a473d-1?X-Amz-Security-Token=IQoJc3JpZ5luX2ZjEAgaCXVzLXdlc3QtNiJHMEUCIQd01onS5L0HoEls2gDBnvn%2B1ICVuRC%2B1qre8Xrrnxq3kAIgfbzEE8r6V9JyhUwUnsGbdSVRF7U%2FbCbSVoQDTgDNa74q0gIIcRBCGgnnNqU2MDc1MTM0ODYiDNOh9BbruKeh2m6pBSqvArF0N8pgmlpcZ8sJTC88qktZMvXEHDkqapjqHxnBzR9U7vBZ6dVjPOqCAZ%2BO%2BzDXCLC07rPxOsRNBxIf%2B3DXAzOZV9XLImWEOXc%2B9h%2FYoGtbM2DR%2BalmYnj667e6yzGL9XxzQqIj%2Ftg0LJtscyd1fjcoyMQ2HeKR7OdknflCK067FwGV22aElb%2FHPsU6eZhqk6jKswqk%2FgFEvw6u%2BYAAErf8Kk5q2yzy1fcJ3aix%2FA7wl0AfkEbuOJaU%2FMun97I%2FMrAqev5SOGp3YVoeXfp1N8QWH0FuCe4nvNkppOoVeDOXbEJlbT%2FvpPsfovmQ4ZTvK%2BGHtwAjkDwhmw1m8yKttYNl2%2Fk%2FZbxlmYhI352qOwNbRSNxZnP34Ht97jUM5G9vnDKMV2jVqcKsaOX7983PMjCLgKn9BTq%2FASF35Axs%2Fk0pyikO0LdmzNj98d7bmeuq4lPVGqgbIJZfC6QEeYw5y5MG05HfeXv0C3fDgvXynsmtbAb%2BJJb8sL8Nz1zSe4GyKFR%9FAhibG31Z%2BeLC1coo%2B6JhWxNLeO43tkTFHuDU450xXe6GLukOmElZqXghUmYh4sOnQkypHz1IWjVJfYYnW5NRDbcQUEmhhG%2B73wJWUEEvQjKSKRBqoNHjfneUnsubvNexAs%2B%2BsMhjhLZN76do1cpCArKyKuKd&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20201110T072355Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=ASIAFARKEUCRGHED4YUR%2F20201110%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=85f6f7d420a19a1e291a21f8f9c6c8aabcc917b9e56809b0b92324ec39285cf5";
```

For example, error contents will look like the following:
```
{"errorMessage":"com.amazonaws.services.iot.model.ResourceRegistrationFailureException: Register thing workflow execution terminates due to: Thing Type 677063384888:luma not found. (Service: AWSIot; Status Code: 400; Error Code: ResourceRegistrationFailureException; Request ID: 600436fb-bf9c-47b4-9011-c9c6f775c920; Proxy: null)","lineNumber":1,"offset":167,"response":null}
{"errorMessage":"com.amazonaws.services.iot.model.ResourceRegistrationFailureException: Register thing workflow execution terminates due to: Thing Type 677063384888:luma not found. (Service: AWSIot; Status Code: 400; Error Code: ResourceRegistrationFailureException; Request ID: dab25d17-8a19-44d0-84ec-3f07a5511667; Proxy: null)","lineNumber":2,"offset":334,"response":null}
```

# Academic Reading

- [A fairly complete documentation of Bulk Provisioning](https://github.com/aws-samples/aws-iot-device-management-workshop/blob/master/AWS_IoT_Device_Management_Workshop.md#bulk-device-provisioning)
