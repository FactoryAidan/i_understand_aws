const Dotenv	=	require('dotenv');	Dotenv.config();
const util		=	require('util')
const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

const template_json		=	require(`${__dirname}/provisioning_template.json`);
const template_policy	=	template_json.Resources.policy.Properties.PolicyDocument;
//	Convert the PolicyDocument to a JSON String - Simple & Straightforward
template_json.Resources.policy.Properties.PolicyDocument	=	JSON.stringify(template_policy);

//	--------------
//	PolicyDocument is a point of contention for most people.
//	FYI:	You CAN'T dynamically create content inside the policy document; it is static.
//			Amazon's Intrinsic functions are not valid in the PolicyDocument (Fn::Join,Fn::Select,Fn::FindInMap,Fn::Split,Fn::Sub)
//	--------------
let example_policyDocument;
//	Valid - An escaped string wrapped in DoubleQuotes
example_PolicyDocument	=	"{ \"Version\": \"2012-10-17\", \"Statement\": [{ \"Effect\": \"Allow\", \"Action\":[\"iot:Publish\"], \"Resource\": [\"arn:aws:iot:us-east-1:123456789012:topic/foo/bar\"] }] }";
//	Valid - An unescaped string wrapped in SingleQuotes
example_PolicyDocument	=	'{ "Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Action":["iot:Publish"], "Resource": ["arn:aws:iot:us-east-1:123456789012:topic/foo/bar"] }] }';
//	Valid - An object stringified
example_PolicyDocument	=	JSON.stringify({ "Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Action":["iot:Publish"], "Resource": ["arn:aws:iot:us-east-1:123456789012:topic/foo/bar"] }] });
//	Not valid - An object
example_PolicyDocument	=	{ "Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Action":["iot:Publish"], "Resource": ["arn:aws:iot:us-east-1:123456789012:topic/foo/bar"] }] };


//	console.log( util.inspect(template_json, false, null, true /* enable colors */) );	return;	//	Debugging Only

(async ()=>{

	const function_params = {
		inputFileBucket	:	'violux-iot-bulk-provisioning',
		inputFileKey	:	'provisioning_data.ndjson',
		roleArn			:	'arn:aws:iam::677063384888:role/IoT_BulkProvisioning_InputSourceS3',
		templateBody	:	JSON.stringify(template_json),
	};
//	console.log(function_params);	return;	//	Debugging Only
	const response	=	await IOT.startThingRegistrationTask(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('Response:',response);

})();
