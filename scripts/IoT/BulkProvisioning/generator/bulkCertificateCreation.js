const Dotenv	=	require('dotenv');	Dotenv.config();
const AWS		=	require('aws-sdk');
const FS		=	require('fs');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();


//	console.log('Dry-run switch is ON; nothing will happen.'); return;	//	Accidental Execution Protection - You do not want to run this accidentally.


//	---------
//	Invoke the certificate creation function repeatedly
//	----------
const count_certificates_desired	=	9500;
for( let counter=0; counter<count_certificates_desired; counter++){
	createKeysAndCertificateThenSaveAsFiles();
}//for



//	---------
//	This will ask AWS to generate a cert & key-pair.
//	It will then place the files from the response onto your filesystem in a `./.certificates/` directory relative to this script.
//	----------
function createKeysAndCertificateThenSaveAsFiles(){
	const function_params = {
		setAsActive	:	true	//	Bool
	};

	IOT.createKeysAndCertificate(function_params).promise()
		.then(function(response){
		//	console.log(response);	//	Debugging Only
			const path	=	`${__dirname}/.certificates/${response.certificateId}/`;

		//	Create a folder to save inside of
			FS.mkdirSync(path,{recursive:true});
		//	Save raw response data (in case we need ARN or ID)
			FS.writeFileSync(`${path}/raw_data.json`,JSON.stringify(response));
		//	Save certificate.pem.crt
			FS.writeFileSync(`${path}/certificate.pem.crt`,response.certificatePem);
		//	Save private.key.pem
			FS.writeFileSync(`${path}/private.key.pem`,response.keyPair.PrivateKey);
		//	Save public.key.pem
			FS.writeFileSync(`${path}/public.key.pem`,response.keyPair.PublicKey);

		//	Console output
			console.log(`Created Certificate.id: ${response.certificateId}`);
		})
		.catch(error=>console.log(Object.entries(error)))
		;

}
