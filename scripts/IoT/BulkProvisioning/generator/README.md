# Thought Process

1. Create **KeyPairs** & **Certificates** so you know the IDs that AWS assigned to those Certificates.
1. Create a `.ndjson` file that maps each **Thing** to a corresponding **Certificate.id** along with any other data you'd like .
1. Upload that `.ndjson` file to a **S3 Bucket**; that's how IoT needs to read-in the file.
1. Start the Bulk Provisioning process using the IoT API. This will give you a task.id so you can check the status or get reports on your bulk provisioning process.
1. That's it, you're done. They're provisioned / or at least there are errors you can read in the reports.

# Bulk Creation of Things

1.	Run `bulkCertificateCreation.js` - after setting how many certificates you want to create in that `.js` file.
1.	Run `bulkProvisioningData.js` - which will create the `.ndjson` file you need to upload to your **AWS S3 Bucket**.
1.	After uploading the `.ndjson` file to your **S3 Bucket**, invoke `.startThingRegistrationTask()` on it.

# Academic Reading

- [Walkthrough](https://aws.amazon.com/blogs/iot/deploy-fleets-easily-with-aws-iot-device-management-services/)
- [IoT Policy Action|Resource mapping]([https://docs.aws.amazon.com/iot/latest/developerguide/iot-action-resources.html)
- [Provisioning Template Example(s)](https://docs.aws.amazon.com/iot/latest/developerguide/provision-template.html)
- [PolicyDocument Conditional Statements](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-conditions.html)
- [PolicyDocument Resource Statement Topics](https://docs.aws.amazon.com/iot/latest/developerguide/device-shadow-mqtt.html)
- [Bulk Provisioning method list](https://docs.amazonaws.cn/en_us/iot/latest/developerguide/bulk-provisioning.html)
- [Fetching Reports from Bulk Provisioning](https://github.com/aws-samples/aws-iot-device-management-workshop/blob/master/AWS_IoT_Device_Management_Workshop.md#bulk-device-provisioning)