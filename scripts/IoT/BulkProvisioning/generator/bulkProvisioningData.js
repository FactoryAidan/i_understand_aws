const FS	=	require('fs');

//	------------------------
//	Get list of Certificates
//	------------------------
const certificate_ids	=	[];
FS.readdirSync(`${__dirname}/.certificates/`).forEach(function(filename){
	certificate_ids.push(filename);
});

//	console.log(certificate_ids);	//	Debugging Only

//	-------------------
//	Create .ndjson file - To upload to S3
//	-------------------
const write_location	=	`${__dirname}/provisioning_data.ndjson`;
const write_stream		=	FS.createWriteStream(write_location);

const begin_at_number	=	500;
for( let counter=0; counter<certificate_ids.length; counter++){
	let row_data	=	{
		ThingName		:	`luma_${begin_at_number+counter}`,
		ThingType		:	'luma',
		ThingGroup		:	'luma_500-to-luma_9999',
		CertificateId	:	certificate_ids[counter],
	};

	write_stream.write(JSON.stringify(row_data)+"\n", 'utf-8');
}//for
// the finish event is emitted when all data has been flushed from the stream
write_stream.on('finish',function(){
	console.log(`.ndjson data for ${certificate_ids.length} IoT 'Things' has been written to:`);
	console.log(write_location);
	console.log('Upload this file to your AWS S3 bucket for Bulk Registration.');
});

// close the stream
write_stream.end();