const FS		=	require('fs');

/*
It's unlikely this script will suit your needs.
A particular client wanted their certificates packaged-up like this.
This is how the factory wanted them delivered so they could automatically flash each individual Printed Circuit Board in the manufacturing line.
*/

//	------------------------
//	Get list of Certificates - from .ndjson file used for BulkRegistration
//	------------------------
const data_ndjson	=	FS.readFileSync(`${__dirname}/provisioning_data.ndjson`);
const data_rows		=	data_ndjson.toString('utf8').trim().split("\n");	//	Buffer to String to Array

//	------------------------
//	Get list of Certificates - from .ndjson file used for BulkRegistration
//	------------------------
data_rows.forEach(function(row_data){
	const row	=	JSON.parse(row_data);
//	console.log(row.CertificateId);	//	Debugging Only
//	console.log(row.ThingName);		//	Debugging Only
	copyAndRenameKeysAndCertificateToDeliverableFolder(row);
});


function copyAndRenameKeysAndCertificateToDeliverableFolder(data){
	//	Prepare destination folder
	const destination_path	=	`${__dirname}/.deliverables/${data.ThingName}`;
	FS.mkdirSync(destination_path,{recursive:true});
	//	Copy Files
	FS.copyFileSync(
		`${__dirname}/.certificates/${data.CertificateId}/certificate.pem.crt`,
		`${destination_path}/${data.ThingName}.certificate.pem.crt`,
	);
	FS.copyFileSync(
		`${__dirname}/.certificates/${data.CertificateId}/private.key.pem`,
		`${destination_path}/${data.ThingName}.private.pem.key`,
	);
}

console.log(`Processed items: ${data_rows.length}`);