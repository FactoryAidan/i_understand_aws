const Dotenv	=	require('dotenv');	Dotenv.config();
const fetch		=	require('node-fetch');
const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.iot	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const IOT	=	new AWS.Iot();

//	CLI Arguments
const arguments	=	process.argv.slice(2);	//	The first two are the parser and the script, so we omit them.

(async ()=>{

	const function_params = {
		reportType	:	'ERRORS|RESULTS',						//	String	required (ERRORS|RESULTS)
		taskId		:	arguments[0] || '4182fa6c-557c-44dd-ac04-5283cd0ba390',	//	String	required
	//	maxResults	:	null,		//	Int
	//	nextToken	:	null,		//	String
	};

	//	Check RESULTS
	function_params.reportType	=	'RESULTS';
	await IOT.listThingRegistrationTaskReports(function_params).promise()
		.then(renderReportUrlContent)
		.catch(error=>console.log(error))
		;

	//	Check ERRORS
	function_params.reportType	=	'ERRORS';
	await IOT.listThingRegistrationTaskReports(function_params).promise()
		.then(renderReportUrlContent)
		.catch(error=>console.log(error))
		;


})();

function renderReportUrlContent(response){
	//	Early Breakpoint(s)
	if( !response )	return;							//	Hmm. This is strange.
	if( !response.resourceLinks.length ) return;	//	No links to fetch

	//	Loop, fetch, & render linked reports.
	const report_urls	=	response.resourceLinks;
	fetch(report_urls.pop())
		.then(response=>response.text())
		.then(function(response){
			//	Response is ndjson format.
			const rows	=	response.trim("\n").split("\n");
			rows.forEach(function(el,i,a){
				console.log(JSON.parse(el));	
			});//forEach
		})
	;//fetch

	return;
}