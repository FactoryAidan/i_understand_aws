const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.s3	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const S3	=	new AWS.S3();

(async ()=>{

	const function_params = {
		Bucket	:	 'staging.blackstonevt.com',
		MaxKeys	:	5
	};
//	console.log(function_params);	return;	//	Debugging Only
	const response	=	await S3.listObjectsV2(function_params).promise()
		.catch(error=>console.log(Object.entries(error)))
		;

	console.log('Response:',response);

})();
