const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.s3	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const S3	=	new AWS.S3();

const object_keys	=	[];

let requests_made	=	0;

(async ()=>{

	const function_params = {
		Bucket				:	 'staging.blackstonevt.com',
		MaxKeys				:	1000,
		ContinuationToken	:	null,
	};
	do {
		await S3.listObjectsV2(function_params).promise()
			.then(response=>{
			//	console.log('Response:',response);
				response.Contents.forEach(function(_object){
					object_keys.push(_object.key);
				});
				function_params.ContinuationToken	=	response.NextContinuationToken;
				requests_made++;
			})
			.catch(error=>{
				console.log(Object.entries(error));
				function_params.ContinuationToken	=	null;
			})
		;
	}while(function_params.ContinuationToken);
	
	console.log(`${object_keys.length} Object Keys from ${requests_made} HTTP requests`);

})();
