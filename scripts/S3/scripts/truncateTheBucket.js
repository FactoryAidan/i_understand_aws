//	----------
//	Note(s):
//	We can only get 1000 objects per list call. We Can only delete 1000 objects per multi-delete call.
//	So we just put the delete call inside the list call's response; deleting whatever is listed per call.
//	----------

const Dotenv	=	require('dotenv');	Dotenv.config();

const AWS		=	require('aws-sdk');

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.s3	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const S3	=	new AWS.S3();

(async ()=>{

//	-------------------
//	Get All Object Keys
//	-------------------
	const function_params = {
		Bucket				:	 'staging.blackstonevt.com',
	//	MaxKeys				:	1000,	//	1000 By Default;
		ContinuationToken	:	null,
	};
	do {
		await S3.listObjectsV2(function_params).promise()
			.then(async function(response){
			//	console.log('Response:',response);
				function_params.ContinuationToken	=	response.NextContinuationToken;

			//	--------------
			//	Delete Objects from this batch
			//	--------------
				const delete_these_objects	=	response.Contents.map(function(_object){return {Key:_object.Key};});
				await S3.deleteObjects({
					Bucket	:	function_params.Bucket,
					Delete	:	{
						Objects	:	delete_these_objects,
					//	Quiet	:	true,	//	Only reports when a deletion error occurs. If no error(s), an empty response is given.
					},
				}).promise()
					.then(response=>{
						console.log('Deletion Response:',response);
					})
					.catch(error=>{console.log(Object.entries(error));})
				;

			})
			.catch(error=>{
				console.log(Object.entries(error));
				function_params.ContinuationToken	=	null;
			})
		;
	}while(function_params.ContinuationToken);

})();
