const FS		=	require('fs');

function listRecursive(start_path='.'){
	const file_list	=	[];
	FS.readdirSync(start_path).forEach(function(filename){
		const pathfilename	=	[start_path,filename].join('/');
		const stat			=	FS.statSync(pathfilename);
		
		const pathfilename_relative	=	pathfilename.replace(/^\.\//,'');
		if( stat && stat.isDirectory() ){
			file_list.push( ...listRecursive(pathfilename_relative) );
		}else{
			file_list.push(pathfilename_relative);
		}//ifelse

	});//forEach
	return file_list;
}//func

module.exports	=	{
	listRecursive,
};