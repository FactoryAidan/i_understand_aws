const Dotenv			=	require('dotenv');	Dotenv.config();
const FS				=	require('fs');
const AWS				=	require('aws-sdk');
const {listRecursive}	=	require(`${__dirname}/filesystemHelper`);

//	IAM credentials needed for admin level privileges on some request endpoints for admin actions.
AWS.config.s3	=	{
	accessKeyId		:	process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey	:	process.env.AWS_SECRET_ACCESS_KEY,
	region			:	process.env.AWS_REGION,
};

const S3	=	new AWS.S3();


const file_list	=	listRecursive('scripts');
//	console.log(file_list);	//	Debugging Only

(async ()=>{

//	----------------
//	Upload All Files
//	----------------
	file_list.forEach(async function(pathfilename){
		const file_buffer	=	FS.readFileSync(pathfilename);
		const function_params = {
			Bucket	:	 'staging.blackstonevt.com',
			Key		:	pathfilename,
			Body	:	file_buffer,
		//	ACL		:	'public-read',
		};

		await S3.upload(function_params).promise()
			.then(async function(response){
				console.log('Response:',response);
			})
			.catch(error=>{
				console.log(Object.entries(error));
			})
		;
	});//forEach
})();
