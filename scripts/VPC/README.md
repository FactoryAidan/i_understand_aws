# 🟢 Question: Lambda not able to connect to 'aws-sdk' Services; it times-out.

#### Academic notes:

- When a Lambda is not assigned to any VPC, it works as desired; it can connect to 'aws-sdk' services.
- However, when a Lambda is assigned to a VPC that contains only 'private' subnets, there's no internet connection. It needs this connection to communicate via the API to fellow AWS services.

#### Resolution:

AWS allows the establishment of an **interface endpoint** to represent in-house services (like `SNS`,`SES`,etc..) for a Lambda to have network access to.
They call it *AWS PrivateLink* in documentation but I've found nothing useful when trying to find answers when searching with that name.

#### How-to:

Here's [AWS's article](https://docs.aws.amazon.com/lambda/latest/dg/configuration-vpc-endpoints.html#vpc-endpoint-create) on how to do it.

If it takes you more than 1 minute to create an interface endpoint, you're not in the right part of the console.

> Note: When entering the **VPC** console, do not select *Endpoint Services*. Rather, select *Endpoints*.


Here are the same steps as written in that ☝️ article:

- Open the *Endpoints* page of the Amazon *VPC* service console.
- Choose *Create Endpoint*.
- Select **Service category**: *AWS services* from the radio buttons.
- Select **Service Name**: `com.amazonaws.region.lambda`. (For example, this would allow a Lambda to user to invoke a different Lambda with `(new AWS.Lambda).invoke()` successfully )
- Verify that the ☝️ **Service Name**'s **Type** is *Interface*.
- Choose a *VPC* and *Subnet(s)* that this service interface endpoint should allow connections from.
- Select **Enable DNS name**: *True* or checkbox is `Checked`.
- For **Security Group**, choose any applicable security groups. *(Your `Default` Security Group is a great one to pick here)*
- Leave **Policy** as *Full Access*.
- Choose *Create Endpoint*.