let AWS = require('aws-sdk');
var mysql2 = require('mysql2');

const do_query	=	"SELECT * FROM submissionsContactForm";

const SETTINGS	=	{
	region		:	'us-east-6',	//	Region of the RDS Proxy (NOT the RDS Instance)
	host_proxy	:	'shared.proxy-xxxxxxxx.us-east-6.rds.amazonaws.com',
	user_rds	:	'shared',		//	The actual username of the RDS Instance; the same username stored in Secrets Manager.
	db_name		:	'SomeDBName',
};

const signer = new AWS.RDS.Signer({
	region	:	SETTINGS.region,
	hostname:	SETTINGS.host_proxy,
	port	:	3306,
	username:	SETTINGS.user_rds
});

exports.handler = async (event) => {

	let connectionConfig = {
		host		:	SETTINGS.host_proxy,
		user		:	SETTINGS.user_rds,
		database	:	SETTINGS.db_name,
		ssl			:	'Amazon RDS',
		authPlugins	:	{ mysql_clear_password: () => () => signer.getAuthToken() }
	};

	const connection	=	mysql2.createConnection(connectionConfig);
	connection.connect();

	connection.query(do_query, function(error,results,fields){
		if( error )	throw error;
		console.log("Ran query: " + do_query);
		for( let result in results )	console.log(results[result]);
	});

	return new Promise(function(resolve, reject){
		connection.end(function(err){
			if( err )    return reject(err);
			const response	=	{
				statusCode	:	200,
				body		:	JSON.stringify({}),
			};
			resolve(response);
		});//connection.end
	});//promise
};//exports.handler
