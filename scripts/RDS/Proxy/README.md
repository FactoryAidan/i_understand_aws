# RDS Proxy Setup

## Thought process

**Lambda** 👉 **RDS Proxy** 👉 **Secrets Manager** 👉 **RDS Instance**

## Gotchas:

> All of these components must be in the same:
> - VPC
> - Subnet(s)
> - Security Group

> The Lambda & RDS Proxy must be in the same:
> - Region

1.	The actual RDS Instance is spoken to by the RDS Proxy
	-	RDS Instance credentials 'can be' stored in AWS Secrets Manager. You can choose not to use the Secrets Manager and simply type the username:password into the Lambda directly.
	-	When using the Secrets Manager, the connection from the RDS Proxy to the RDS Instance is authenticated with via IAM.
	-	*Note: The security group must contain an inbound rule that allows itself.*
1.	The RDS Proxy is spoken to by the Lambda

[RDS Proxy Limitations](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/rds-proxy.html#rds-proxy.limitations)
-	**Not compatible with MySQL 8 !** ☝️

# Setup

# 🟢 EC2: Security Group

-	Create a security group called `lambda_rds_proxy` and give it an *Inbound rule* that allows itself. *(MYSQL/Aurora,TCP,3306,sg-xxxxxx)*

You will eventually attach the following to this **Security Group**.

-	RDS Instance
-	RDS Proxy
-	Lambda

# 🟢 Secrets Manager

Create a new secret that stores your database's `username:password`.


# 🟢 IAM Role & Policy: for RDS Proxy to read from the Secrets Manager

#### Policy

Create an IAM Policy called `RDSProxy_ReadSecret_MyDb`.

Set the following as its Policy Document:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "GetSecretValue",
            "Action": [
                "secretsmanager:GetSecretValue"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:secretsmanager:us-west-1:XXXXXXXXXXXX:secret:RDS/leuser-w11AfT"
            ]
        },
        {
            "Sid": "DecryptSecretValue",
            "Action": [
                "kms:Decrypt"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:kms:us-west-1:XXXXXXXXXXXX:key/2014e57d-59dd-8402-ac61-747ff146d990"
            ],
            "Condition": {
                "StringEquals": {
                    "kms:ViaService": "secretsmanager.us-west-1.amazonaws.com"
                }
            }
        }
    ]
}
```

#### Role

Create an IAM role called `RDSProxy_ReadSecret_MyDB`.

Its *use case* should be 'rds'.

Attach the **Policy** you just created to it ☝️.
(**If the AWS Web Console doesn't let you see the Policy you created, you can set Use Case to `Lambda` and change the Trusted Entity to `rds` after creation.**)



###### Give it a *Trust Relationship* containing the following document:
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "rds.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```

After creation, this **Role** should be listed in the Roles table with **Trusted Entity** as `AWS service: rds`.

# 🟢 RDS Instance

Make sure your RDS Instance:

-	Is set to use both *Password and IAM database authentication*; However your Lambda will only be using IAM.
-	Is set to be within the same *VPC*, *Subnet(s)*, and the *Security Group* you made above of the *RDS Proxy* & *Lambda*.

Notes:

-	The RDS Instance does not need to be *Publicly Accessible*

# 🟢 RDS Proxy

Make sure your RDS Proxy:

-	Is set to **Require Transport Layer Security**.
-	Is set to use the **Secrets Manager** secret you created above.
-	Is set to use the **IAM Role** you created above for the RDS Proxy to access the Secrets Manager.
-	Is set to have **IAM authentication** as *Required*.
-	Is set to be within the same **VPC**, **Subnet(s)**, and the **Security Group** you made above of the **RDS Instance** & **Lambda**.


# 🟢 Lambda

-	Is set to be within the same **VPC**, **Subnet(s)**, and the **Security Group** you made above of the **RDS Instance** & **RDS Proxy**.
-	Is set to have **IAM authentication** as **Required**.

The following code will work if you've setup everything as AWS likes it:

Note: *We're using npm package `mysql2`, so you'll have to npm install it locally, zip your files, and upload them to the Lambda.*

```
let AWS = require('aws-sdk');
var mysql2 = require('mysql2');

const do_query	=	"SELECT * FROM submissionsContactForm";

const SETTINGS	=	{
	region		:	'us-west-1',	//	Region of the RDS Proxy (NOT the RDS Instance)
	host_proxy	:	'shared.proxy-xxxxxxxxxxxx.us-west-1.rds.amazonaws.com',
	user_rds	:	'shared',		//	The actual username of the RDS Instance; the same username stored in Secrets Manager.
	db_name		:	'SomeDBName',
};

const signer = new AWS.RDS.Signer({
	region	:	SETTINGS.region,
	hostname:	SETTINGS.host_proxy,
	port	:	3306,
	username:	SETTINGS.user_rds
});

exports.handler = async (event) => {

	let connectionConfig = {
		host		:	SETTINGS.host_proxy,
		user		:	SETTINGS.user_rds,
		database	:	SETTINGS.db_name,
		ssl			:	'Amazon RDS',
		authPlugins	:	{ mysql_clear_password: () => () => signer.getAuthToken() }
	};

	const connection	=	mysql2.createConnection(connectionConfig);
	connection.connect();

	connection.query(do_query, function(error,results,fields){
		if( error )	throw error;
		console.log("Ran query: " + do_query);
		for( let result in results )	console.log(results[result]);
	});

	return new Promise(function(resolve, reject){
		connection.end(function(err){
			if( err )    return reject(err);
			const response	=	{
				statusCode	:	200,
				body		:	JSON.stringify({}),
			};
			resolve(response);
		});//connection.end
	});//promise
};//exports.handler
```