global.fetch				=	require('node-fetch');
const fetchIntercept		=	require('fetch-intercept');
const {AwsCognitoHelper}	=	require('../../../AwsHelpers.js');

const {Auth}	=	require('@aws-amplify/auth');
//const Amplify	=	require('aws-amplify');
//const {Auth}	=	Amplify;

const Dotenv	=	require('dotenv');	Dotenv.config({path:`${__dirname}/../.env`});


//console.log(process.env);

attachFetchInterceptor();

//	https://docs.amplify.aws/lib/auth/emailpassword/q/platform/js#sign-up
Auth.configure({

	//	REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
//	identityPoolId		:	'XX-XXXX-X:XXXXXXXX-XXXX-1234-abcd-1234567890ab',

	//	REQUIRED - Amazon Cognito Region
	region				:	process.env.AWS_COGNITO_REGION,

	//	OPTIONAL - Amazon Cognito Federated Identity Pool Region 
	// Required only if it's different from Amazon Cognito Region
//	identityPoolRegion	:	process.env.AWS_COGNITO_IDENTITY_POOL_REGION,

	//	OPTIONAL - Amazon Cognito User Pool ID
	userPoolId			:	process.env.AWS_COGNITO_USER_POOL_ID,

	//	OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
	userPoolWebClientId	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,

	//	OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
	mandatorySignIn		:	false,

	// OPTIONAL - Configuration for cookie storage
	// Note: if the secure flag is set to true, then the cookie transmission requires a secure protocol
/*
	cookieStorage		:	{
		// REQUIRED - Cookie domain (only required if cookieStorage is provided)
		domain	:	'.yourdomain.com',
		// OPTIONAL - Cookie path
		path	:	'/',
		// OPTIONAL - Cookie expiration in days
		expires	:	365,
		// OPTIONAL - See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite
		sameSite	:	"strict" | "lax",
		// OPTIONAL - Cookie secure flag
		// Either true or false, indicating if the cookie transmission requires a secure protocol (https).
		secure	:	true
	},
*/
	// OPTIONAL - customized storage object
//	storage					:	MyStorage,

	// OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
//	authenticationFlowType	:	'USER_PASSWORD_AUTH',


	// OPTIONAL - Manually set key value pairs that can be passed to Cognito Lambda Triggers
/*
	clientMetadata			:	{
		myCustomKey	:	'myCustomValue'
	},
*/

	// OPTIONAL - Hosted UI configuration
/*
	oauth: {
		domain			:	'your_cognito_domain',
		scope			:	['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
		redirectSignIn	:	'http://localhost:3000/',
		redirectSignOut	:	'http://localhost:3000/',
		responseType	:	'code' // or 'token', note that REFRESH token will only be generated when the responseType is code
	}
*/
});

//	You can get the current config object
//	const currentConfig = Auth.configure();
//	console.log(currentConfig);	return;

(async ()=>{

		//	------
		//	signUp
		//	------
		await Auth.signUp({
			username	:	process.env.A_TEST_EMAIL_ADDRESS,
			password	:	process.env.A_TEST_PASSWORD,
			attributes	:	{
				email	:	process.env.A_TEST_EMAIL_ADDRESS,
			},
		})
		/*
		.then(response=>{

			console.log('signUp():')
			console.log(response);
			return Auth.signIn(process.env.A_TEST_EMAIL_ADDRESS,process.env.A_TEST_PASSWORD)
		})
		*/
		.then(response=>{
			console.log('signUp():',response);
			return response;
		})
		.catch(error=>console.log(error))
		;
		
		//	------
		//	signIn
		//	------
		//	NOTE:	There's an issue with aws-amplify and this signIn() call where SRP fails.
		//	However, the vanilla implementation succeeds in this repository:
		//		-	./scripts/Cognito/user/signIn/secure_remote_password/initiateAuth-user_srp_auth.using_library.js
		await Auth.signIn(process.env.A_TEST_EMAIL_ADDRESS,process.env.A_TEST_PASSWORD).then(response=>{
			console.log('signIn():')
			console.log(response);
			return Auth.signIn(process.env.A_TEST_EMAIL_ADDRESS,process.env.A_TEST_PASSWORD)
		})
		.catch(error=>console.log(error))
		;

		//	----
		//	Misc
		//	----		
		//	In the interest of answering the question if signUp() also signs-in the user, we look at the following:
		//	If user is created / confirmed but not signed-in, we'll currentSession() return:
		//		"No current user"
	//	console.log( Amplify.Auth.currentSession() );

})();


function attachFetchInterceptor(){
	this.unregister_intercept	=	fetchIntercept.register({
	
	/*
		response: async function(response){
			console.log('RESPONSE:', await response.json());
			// Modify the reponse object
			return response;
		},
	*/
		request: function(url, config){

			const { headers } = config;

		//	console.log(headers);
	
			if( headers && headers['X-Amz-Target'].indexOf('AWSCognitoIdentityProviderService') != -1 ){

				const body	=	JSON.parse(config.body);
				
			//	console.log(body);

				
				//	I know. There are some shared Target's here and I don't need the if(){} statement on the User-Agent at all.
				//	I'll take it out eventually.
				if( /^aws-amplify\//i.test(headers['X-Amz-User-Agent']) ){
					//	We're using 'aws-amplify'

					switch( headers['X-Amz-Target'] ){
						case 'AWSCognitoIdentityProviderService.SignUp':
								body.SecretHash	=	getSecretHash(body.Username);
								break;
						case 'AWSCognitoIdentityProviderService.InitiateAuth':
								body.AuthParameters.SECRET_HASH	=	getSecretHash(body.AuthParameters.USERNAME);
								break;
						case 'AWSCognitoIdentityProviderService.RespondToAuthChallenge':
								body.ChallengeResponses.SECRET_HASH	=	getSecretHash(body.ChallengeResponses.USERNAME);
								break;
						default:
					}//switch

				}else{
					//	We're using 'amazon-cognito-identity-js'
					switch( headers['X-Amz-Target'] ){
						case 'AWSCognitoIdentityProviderService.InitiateAuth':
								body.AuthParameters.SECRET_HASH	=	getSecretHash(body.AuthParameters.USERNAME);
								break;
						case 'AWSCognitoIdentityProviderService.RespondToAuthChallenge':
								body.ChallengeResponses.SECRET_HASH	=	getSecretHash(body.ChallengeResponses.USERNAME);
								break;
						default:
					}//switch				
				
				}//ifelse

			//	console.log(body);

				config.body = JSON.stringify(body)
			}//if
			return [url, config];

			//	Username can change when using SRP Auth. This function is to adapt to that change.
			function getSecretHash(given_username){
			//	console.log('USERNAME',given_username);
				const secret_hash	=	AwsCognitoHelper.calculateSecretHash({
					user_pool_app_client_id		:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
					user_pool_app_client_secret	:	process.env.AWS_COGNITO_USER_POOL_APP_CLIENT_SECRET,
					user_pool_user_name			:	given_username,
				});
				return secret_hash;
			}//func

		},
	});
	
}//function

