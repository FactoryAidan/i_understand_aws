# Deprecated

Do not use 'aws-amplify' with Node.js . It does not support the 'SECRET_HASH' header using window.fetch().
While it is possible to use 'fetch-intercept' to add that header, it is less work to simply use 'aws-sdk' for Cognito functionality.